#!/usr/bin/python



import os
import requests
import threading
import Queue
from math import exp, log
from time import time, sleep
from random import randint
from bid_perform import perform_bid_from_id, perform_bid
from socketIO_client import SocketIO
from query import *

socket_server_host = 'localhost'
socket_server_port = 8181
node_server = 'http://localhost:2048'
# if this is set to true the time is simulated instead of waiting for the actual length
simulation = False
# transmit bids via sockets to node
transmit_bids = True
# insert into bid history database
log_bids = True


# wait at least a few seconds between bids
bottom_wait_limit = 6
# wait at most this percentage of the remaining time in the bid window
top_wait_limit = .3



class BidProcessor(threading.Thread):

    def __init__(self, queue):
        self.__queue = queue
        threading.Thread.__init__(self)
        self.daemon = True


    def run(self):
        while 1:
            curr_time = time()
            bid_item = self.__queue.get()
            (next_bid_time, query, time_remaining) = bid_item

            # bid time is close, perform it
            if next_bid_time - curr_time < 1:
                self.process_bids(bid_item, curr_time)

            # bid is still in the future, put it off for now
            else:
                self.__queue.put(bid_item)
            sleep(randint(500, 1000) / 1000.0)



    # perform a bidding round, log the results in database and send to node
    def process_bids(self, bid_item, curr_time):
        (next_bid_time, query, time_remaining) = bid_item
        print 'task', bid_item, 'being processed.',
        print '(queue size: %d)' % self.__queue.qsize()

        # try to get updated query data
        query = query.update()

        # do the current round of bidding and figure out when the next round should occur
        current_bids = perform_bid(query, query.query_id())

        wait_time = compute_wait_time(time_remaining, query.completed_bidding_rounds(), bottom_wait_limit, top_wait_limit)

        # send the bid results to node
        for bid in current_bids.values():
            print '%s time_remaining: %d wait_time: %d' % (bid, time_remaining, wait_time)

            # save the bid in mysql
            if log_bids:
                r = requests.post(node_server + '/bid_history', data = bid.as_json_for_insertion())

            # send to node which will forward to client device
            if query.completed_bidding_rounds() > 1 and transmit_bids:
                with SocketIO(socket_server_host, socket_server_port) as socketIO:
                    socketIO.emit('bid_recv', { "user_id": query.user_id(), \
                            "query_id":query.query_id(), \
                            "price_now" : bid.new_price(), \
                            "inventory":bid.inventory(), \
                            "deal_id":bid.deal_id()})


        # queue up the next bidding round
        if time_remaining > bottom_wait_limit:
            time_remaining = time_remaining - wait_time

            if simulation:
                wait_time = 2

            next_bid_time = time() + wait_time
            print 'queueing next bid round for query %s' % query.query_id()
            self.__queue.put((next_bid_time, query, time_remaining))
        else:
             print 'window for query %s expired. %d bidding rounds.'  \
            % (query.query_id(), query.completed_bidding_rounds())



            
            
"""
given time remaining in the bid window, etc, compute the amount of time to wait until the next bid round
"""
def compute_wait_time(time_remaining, completed_bidding_rounds, bottom_wait_limit, top_wait_limit):

    # make sure there bids within the first minute
    if completed_bidding_rounds < 5:
        return randint(bottom_wait_limit, 10)

    # compute the time to sleep before the next bid
    wait_time = exponential_clock(time_remaining)
    while wait_time > time_remaining:
        wait_time = int(round(exponential_clock(time_remaining)))
        #print 'recomputing wait time'

    wait_time = min(time_remaining * top_wait_limit, wait_time)
    wait_time = max(bottom_wait_limit, wait_time)

    return wait_time



"""
draw from the exponential distribution using inverse transform sampling
"""
def exponential_clock(_lambda):
    return - log(random()) * 10 *  _lambda
