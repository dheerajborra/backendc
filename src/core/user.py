#!/usr/bin/python

import simplejson as json

"""
our model of a user
"""

class Sex:
    UNKNOWN = "U"
    FEMALE = "F"
    MALE = "M"


class User:
    __uid = -1
    __firstname = ""
    __lastname = ""
    # should be enum
    __sex = Sex.UNKNOWN
    __karma = -1
    __email = ""
    # take proper precautions here
    __password = ""
    __phonenumber = ""
    __birthdate = ""
    __flags = ""
    __facebook_id = ""
    __facebook_token = ""
    

    def __init__(self, uid, firstname, lastname, sex, karma, email, password, phonenumber, birthdate, flags, facebook_id, facebook_token):
        self.__uid = uid
        self.__firstname = firstname
        self.__lastname = lastname
        self.__sex = sex
        self.__karma = karma
        self.__email = email
        self.__password = password
        self.__phonenumber = phonenumber
        self.__birthdate = birthdate
        self.__flags = flags
        self.__facebook_id = facebook_id
        self.__facebook_token = facebook_token
        
       
    # unpack tuple and pass to constructor
    @classmethod
    def from_tuple(cls, values):
        (uid, firstname, lastname, sex, karma, email, \
        password, phonenumber, birthdate, flags, facebook_id, facebook_token) = values
        return User(uid, firstname, lastname, sex, karma, email, \
                password, phonenumber, birthdate, flags, facebook_id, facebook_token)
    

    def uid(self):
        return self.__uid

    
    def email(self):
        return self.__email


    def as_json(self):
        r = self.__dict__
        dct = {}
        for key in r:
            dct[key.replace('_User__', '')] =  r[key]
        return dct

    # pass json without a uid value
    def as_json_for_insertion(self):
        dct = self.as_json();
        dct.pop('uid', None)
        return dct

    def encode_json(self):
        return json.JSONEncoder(default=User.as_json).encode(self)



    @classmethod
    def as_user(cls, dct):
        return User(dct['uid'], dct['firstname'], dct['lastname'], \
                dct['sex'], dct['karma'], dct['email'],  \
                dct['password'], dct['phonenumber'],\
                dct['birthdate'], dct['flags'], dct['facebook_id'],\
                dct['facebook_token'])


    # construct a user object from json string
    @classmethod
    def decode_json(cls, json_str):
        return json.loads(json_str, object_hook=User.as_user)
    

    # string of values to be inserted into db.
    # uid is missing because it is handled by db
    def attrs(self):
        return "(firstname, lastname, sex, karma, email, password, phonenumber, birthdate, flags, facebook_id, facebook_token)"


    # return a tuple of attributes
    def values(self):
        return (self.__uid, self.__firstname, self.__lastname, \
                self.__sex, self.__karma, self.__email, \
                self.__password, self.__phonenumber, self.__birthdate, \
                self.__flags, self.__facebook_id, self.__facebook_token)
    

    def __str__(self):
        return "uid: %d firstname: %s lastname: %s sex: %s karma: %d email: %s facebook_id: %s" % \
            (self.__uid, self.__firstname, self.__lastname, self.__sex, self.__karma, self.__email, self.__facebook_id)
