#!/usr/bin/python

import urllib
import urllib2
import datetime

from random import random as rand
from deal import *
from random import random
from time import time


"""
our model of a query

@author: tomas mccandless, vaibhav gupta
tdm@azul.me, v@azul.me
"""

class Query:
    __data = {}
    # keyed by dealid
    __deals = {}
    __demands = {}
    __completed_bidding_rounds = 0
    __query_id = None
    # store the last bid for each deal id
    __last_bids = {}

    """
    __queryid = -1
    __user = None
    __deals = {}
    __demands = {}
    __favorites = set()
    __trash = set()
    __iterations = 0
    __bidtimes = []
    __bidCounter = []
    __totalBids = 0
    __search_str = ""
    """    

    def __init__(self, data, query_id=None):

        # convert from unicode to ascii
        if type(data) == type(u''):
            data = data.encode('ascii', 'ignore') 

        if type(data) == type(''):
            data = json.loads(data, encoding='ascii')

        if 'query_id' in data:
            self.__query_id = data['query_id']

        if query_id is not None:
            self.__query_id = query_id

        data = data['query']

        for deal_dict in data['deals']:
            deal = Deal(deal_dict)
            self.__deals[deal.deal_id()] = deal
        self.__data = data


    # TODO bid windows are attached to deal ids, move them to the query level in the 
    # response sent back from es
    def bid_window(self):
        deals = self.val_or_none('deals')
        if not deals:
            return None
        
        return None if 'bid_window' not in deals[0] else deals[0]['bid_window']



    # return the number of seconds left before the end of the bid window
    def time_remaining(self):
        date_time = self.bid_window()
        frmt = '%Y-%m-%d %H:%M:%S'
        bid_window_end=datetime.datetime.strptime(date_time, frmt)
        bid_window_start = datetime.datetime.now()

        return (bid_window_end - bid_window_start).total_seconds()




    @classmethod
    def from_query_id(cls, query_id):
        es_query = {"type":"query", "data": {"id": str(query_id)}}
        response = urllib2.urlopen('http://128.83.196.226:2049/search/', urllib.urlencode(es_query))
        t = response.read()
        # print 'response from es server: %s' % t

        if ('query' not in t):
            return 'error: query id not found?'

        else:
            return Query(t, query_id)




    def update(self):
        updatedquery = Query.from_query_id(self.query_id())
        if type(updatedquery) == type(self):
            self.__data = updatedquery.__data
            self.__deals = updatedquery.__deals

        return self


    def add_last_bid(self, deal_id, bid_obj):
        self.__last_bids[deal_id] = bid_obj


    
    def get_last_bid(self, deal_id):
        if deal_id in self.__last_bids:
            return self.__last_bids[deal_id]
        else:
            return None


    def get_demands(self):
        demands = {}

        for deal_id in self.__deals:
            # db call here
            curr_deal = self.__deals[deal_id]
            i = random() > 0.75
            demands[deal_id] = (int(i) + random()) * curr_deal.inventory()

        self.__demands = demands

    def val_or_none(self, key):
        return None if key not in self.__data else self.__data[key]


    def deals(self):
        return self.__deals.values()

    def completed_bidding_rounds(self):
        return self.__completed_bidding_rounds

    def inc_bid_rounds(self):
        self.__completed_bidding_rounds += 1


    # return the id of the user that made this query
    def user_id(self):
        return str(self.val_or_none('user_id'))


    def query_id(self):
        if self.__query_id:
            return self.__query_id
        else: 
            return self.val_or_none('query_id')

    def __str__(self):
        return str(self.__data)


    def demand(self, deal_id):
        return self.__demands[deal_id]
