#!/usr/bin/python

import datetime
import urllib
import urllib2
import json
import sys
from random import random, randint, sample, uniform
from bid import *
from user import *
from query import *
from time import time

"""
functions to perform bidding. 

@author: tomas mccandless, vaibhav gupta
tdm@azul.me, v@azul.me
"""

denorm_price = lambda ratio, max_p, min_p: float(ratio) * (max_p - min_p) + min_p


market_ratio_cap = 1
decrement_freq_pct = .8
# decrements are chosen uniformly at random between these two values
decrement_pct_min = .05
decrement_pct_max = .2
# increments are chosen uniformly at random between these two values
increment_pct_min = .03
increment_pct_max = .1
max_num_bids_per_round = 3

compute_decrement_pct = lambda: random() * (decrement_pct_max - decrement_pct_min) + decrement_pct_min
compute_increment_pct = lambda: random() * (increment_pct_max - increment_pct_min) + increment_pct_min

# given a query object, attempt a bid on each of the relevant deals
# return new prices for each deal
def perform_bid(query_obj, query_id):

    # update the demands for each relevant deal
    query_obj.get_demands()
    # sort deals by their relevance for this particular query
    deals = query_obj.deals()
    
    current_bids = {}

    # first time around we need to set up the initial bids at max price
    if query_obj.completed_bidding_rounds() == 0:
        for deal in deals:
            deal_id = deal.deal_id()
            bid = Bid(0, True, query_obj.user_id(), deal_id, deal.max_price(), deal.max_price(), deal.inventory(), query_id, time())

            query_obj.add_last_bid(deal_id, bid)
            current_bids[deal_id] = bid


    # otherwise, just pick some random deals to bid on
    else:
        num_bids = randint(1, min(max_num_bids_per_round, len(deals)))
        #print 'bidding on %d deals' % num_bids
        deal_indices = sample(xrange(len(deals)), num_bids)

        for deal_index in deal_indices:

            deal = deals[deal_index]
            deal_id = deal.deal_id()
            supply = deal.inventory()
            demand = query_obj.demand(deal_id)
            last_bid = query_obj.get_last_bid(deal_id)

            market_ratio = float(supply) / demand
            #price_ratio = float(last_bid.new_price() - deal.min_price()) / (deal.max_price() - deal.min_price())
            old_price = last_bid.new_price()
            amt_above_min = last_bid.new_price() - deal.min_price()

            # if we're already at min price, this is needed for proper increments
            if amt_above_min == 0:
                amt_above_min = uniform(0.05, 0.1)

            if (last_bid.new_price() == deal.max_price()) or (market_ratio > market_ratio_cap and random() < decrement_freq_pct):
                # decrement price
                new_ratio = -compute_decrement_pct()
            else:
                # increment price, by 2% at first
                new_ratio = compute_increment_pct()


            new_price = old_price + new_ratio * amt_above_min
            next_bid = Bid(0, False, query_obj.user_id(), deal_id, old_price, new_price, supply, query_id, time())

            query_obj.add_last_bid(deal_id, next_bid)
            current_bids[deal_id] = next_bid


    # track that we've finished another bidding round
    query_obj.inc_bid_rounds()
    return current_bids




def perform_bid_from_id(query_id):
    q =  Query.from_query_id(query_id)
    if type(q) != type(''):
        return perform_bid(q)




# use this function to test how bidding behaves. dont call in production.
# simulates a bid request every interval seconds.
# a bid request attempts a bid for each deal relevant for this query.
# whether a bid happens for a particular deal d is controlled by exponential clock
# based on how long in the past there was a successful bid for d
def perform_bids(query_obj):
    interval = 5
    num_requests = 100

    for i in xrange(num_requests):
        perform_bid(query_obj)
        print 'done with bid request %d' % i
        sleep(interval)
