#!/usr/bin/python

"""
our model of a bid
this is intended to be a lightweight way for us to track bidding history
every time a new bid is performed in bid_perform()
we'll log the update in mysql,
eventually perform some offline processing

author: tomas mccandless
tdm@azul.me
"""

class Bid:
    # unique id for this bid
    __bid_id = -1
    # which user is this bid for
    __user_id = -1
    # which deal is this bid for
    __deal_id = -1
    # we can get merchant from dealid so im not storing anything else
    __old_price = -1.0
    __new_price = -1.0
    # track whether this bid was initial for this query
    __initial_bid = False
    __time = -1
    __timestamp = -1
    __flags = set()
    __inventory = -1
    __query_id = -1
    # TODO anything else to be stored?


    # time is optional because it is handled by the database
    # TODO make bidid optional for the same reason, and do the same for other 
    # unique identifiers.
    def __init__(self, bid_id, initial, user_id, deal_id, old_price, new_price, inventory, query_id, time=-1):
        self.__bid_id = bid_id
        self.__initial_bid = initial
        self.__user_id = user_id
        self.__deal_id = deal_id
        self.__old_price = old_price
        self.__new_price = new_price
        self.__inventory = inventory
        self.__query_id = query_id
        self.__time = time

    def query_id(self):
        return self.__query_id

    def timestamp(self):
        return self.__timestamp

    def new_price(self):
        return self.__new_price

    def deal_id(self):
        return self.__deal_id
    
    def inventory(self):
        return self.__inventory

    def as_json(self):
        r = self.__dict__
        dct = {}
        for key in r:
            dct[key.replace('_Bid__', '')] =  r[key]
        return dct


    def as_json_for_insertion(self):
        dct = self.as_json();
        dct.pop('bid_id', None)
        dct.pop('time', None)
        initial = dct['initial_bid']
        dct['initial_bid'] = 1 if initial else 0
        return dct


    def encode_json(self):
        return json.JSONEncoder(default=Bid.as_json).encode(self)

    # construct a user object from json string
    @classmethod
    def decode_json(cls, json_str):
        return json.loads(json_str, object_hook=Bid.as_bid)


    @classmethod
    def as_bid(cls, dct):
        return Bid(dct['bid_id'], dct['initial'], dct['user_id'], dct['deal_id'], dct['old_price'], \
                dct['new_price'], dct['inventory'], dct['query_id'], dct['time'])


    def __str__(self):
        s = "bid: user: %s deal: %s old_price: %1.2f new_price: %1.2f" % \
                (str(self.__user_id), self.__deal_id, self.__old_price, self.__new_price)
        t = ''
        if self.__initial_bid:
            t = '>>'
        elif self.__new_price < self.__old_price:
            t = '--'
        else:
            t = '++'

        return t + ' ' + s
