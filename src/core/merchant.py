#!/usr/bin/python

import simplejson as json

"""
our model of a merchant
"""

class Merchant:
    __uid = -1
    __name = ""
    __address = ""
    __latitude = -1
    __longitude = -1
    __categories = []
    __phonenumber = ""
    __email = ""
    # number of stars, probably fix at the .5 granularity
    __rating = -1
    # average $$
    __price = -1
    # special events, etc. to be used in weighting search results
    __flags = ""
    
    def __init__(self, uid, name, address, latitude, longitude, categories, phone, email, rating, price, flags):
        self.__uid = uid
        self.__name = name
        self.__address = address
        self.__latitude = latitude
        self.__longitude = longitude
        self.__categories = categories
        self.__phonenumber = phone
        self.__email = email
        self.__rating = rating
        self.__price = price
        self.__flags = flags
        
    # unpack tuple and pass to constructor
    @classmethod
    def from_tuple(cls, values):
        (uid, name, address, latitude, longitude, \
         phone, email, rating, price, flags) = values
        return cls(uid, name, address, latitude, longitude, [], phone, email, rating, price, flags)

    # use address to compute lat/long coordinates
    def compute_coordinates(self):
        pass

    
    def email(self):
        return self.__email


    def merchant_id(self):
        return self.__uid


    def as_json(self):
        r = self.__dict__
        dct = {}
        for key in r:
            dct[key.replace('_Merchant__', '')] =  r[key]
        return dct

    # pass json without a uid value
    def as_json_for_insertion(self):
        dct = self.as_json();
        dct.pop('uid', None)
        dct.pop('categories', None)
        return dct


    def encode_json(self):
        return json.JSONEncoder(default=Merchant.as_json).encode(self)


    @classmethod
    def json_as_merchant(cls, dct):
        return Merchant(dct['uid'], dct['name'], dct['address'], dct['latitude'],\
                dct['longitude'], dct['categories'], dct['phonenumber'], dct['email'], \
                dct['rating'], dct['price'], dct['flags'])


    # construct a user object from json string
    @classmethod
    def decode_json(cls, json_str):
        return json.loads(json_str, object_hook=Merchant.json_as_merchant)
    
    def attrs(self):
        return "(name, address, latitude, longitude, phonenumber, email, rating, price, flags)"
    
    def values(self):
        return (self.__uid, self.__name, self.__address, \
                self.__coordinates[0], \
                self.__coordinates[1], self.__phone, \
                self.__email, self.__rating, self.__price, self.__flags)

    def __str__(self):
        return "id: %d name: %s address: %s email: %s price: %1.1f" % \
                (self.__uid, self.__name, self.__address, self.__email, self.__price)
