#!/usr/bin/python

"""
our model of a deal.
should be stored in elasticsearch.

@author: tomas mccandless
tdm@azul.me
"""

import json
from random import randint 

class Deal:
    # lots of fields, many of which are optional, so just store everything in a dict
    __data = {}


    def __init__(self, data):
        # convert from unicode to ascii
        if type(data) == type(u''):
            data = data.encode('ascii', 'ignore') 

        if type(data) == type(''):
            data = json.loads(data, encoding='ascii')

        assert 'deal_id' in data
        assert 'max_price' in data
        assert 'min_price' in data
        self.__data = data


    # TODO move to commons
    def val_or_none(self, key):
        return None if key not in self.__data else self.__data[key]
        
    def deal_id(self):
        return self.val_or_none('deal_id')

    def merchant_id(self):
        return self.val_or_none('merchant_id')

    def redemption_expiration(self):
        return self.val_or_none('redemption_expiration')

    def redemption_start_time(self):
        return self.val_or_none('redemption_start_time')

    def redemption_end_time(self):
        return self.val_or_none('redemption_end_time')

    def inventory(self):
        return self.val_or_none('inventory')

    def start_date(self):
        return self.val_or_none('start_date')

    def end_date(self):
        return self.val_or_none('end_date')

    def max_price(self):
        return self.val_or_none('max_price')

    def min_price(self):
        return self.val_or_none('min_price')

    def reg_price(self):
        return self.val_or_none('reg_price')

    # only exists when this deal exists in the context of a query for a user
    def last_bid_time(self):
        return self.val_or_none('last_bid_time')

    def name(self):
        return self.val_or_none('name')

    def categories(self):
        return self.val_or_none('categories')

    def days(self):
        return self.val_or_none('days')

    def city(self):
        return self.val_or_none('city')

    def locations(self):
        return self.val_or_none('locations')

    def max_per_user(self):
        return self.val_or_none('max_per_user')

    def description(self):
        return self.val_or_none('description')

    def fine_print(self):
        return self.val_or_none('fine_print')

    def bid_type(self):
        return self.val_or_none('bid_type')

    @classmethod
    def mock_deals(cls):
        d0 = {"reg_price": 5, "deal_id": "0", "end_date": "2013-08-28 08:30:00",
            "redemption_start_time": "08:30:00", 
            "redemption_expiration": "2013-08-28 08:30:00", 
            "num_deals": randint(5,25),
            "start_date": "2013-07-28 08:30:00",
            "min_price": 5,
            "redemption_end_time": "22:30:00",
            "last_bid_time": "1990-01-01 00:00:00",
            "merchant_id": 1,
            "max_price": 10}
        d1 = {"reg_price": 5, "deal_id": "1", "end_date": "2013-08-28 08:30:00",
            "redemption_start_time": "08:30:00", 
            "redemption_expiration": "2013-08-28 08:30:00", 
            "num_deals": randint(5,25),
            "start_date": "2013-07-28 08:30:00",
            "min_price": 10,
            "redemption_end_time": "22:30:00",
            "last_bid_time": "1990-01-01 00:00:00",
            "merchant_id": 1,
            "max_price": 20}
        d2 = {"reg_price": 5, "deal_id": "2", "end_date": "2013-08-28 08:30:00",
            "redemption_start_time": "08:30:00", 
            "redemption_expiration": "2013-08-28 08:30:00", 
            "num_deals": randint(5,25),
            "start_date": "2013-07-28 08:30:00",
            "min_price": 15,
            "redemption_end_time": "22:30:00",
            "last_bid_time": "1990-01-01 00:00:00",
            "merchant_id": 1,
            "max_price": 30}
        d3 = {"reg_price": 5, "deal_id": "3", "end_date": "2013-08-28 08:30:00",
            "redemption_start_time": "08:30:00", 
            "redemption_expiration": "2013-08-28 08:30:00", 
            "num_deals": randint(5,25),
            "start_date": "2013-07-28 08:30:00",
            "min_price": 20,
            "redemption_end_time": "22:30:00",
            "last_bid_time": "1990-01-01 00:00:00",
            "merchant_id": 1,
            "max_price": 40}
        return {'0':Deal(d0),
                '1':Deal(d1),
                '2':Deal(d2),
                '3':Deal(d3)}

        
    def to_json(self):
        return json.dumps(self.__data)

    def __str__(self):
        return self.to_json()
