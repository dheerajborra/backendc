#!/usr/bin/python

"""
use this object to track a users favorite or trashed
deals or merchants

a user marks a deal or merchant as either a favorite or trash

this allows us to keep our schema normalized

author: tomas mccandless
tdm@azul.me
"""

# TODO track timestamp also?
class UserMark:
    # the id of the user 
    __userid = -1
    # the id of the deal or merchant
    __otherid = -1
    # booleans to track whether this is favorite or trash
    # and linked to a deal or merchant
    __is_favorite = False
    __is_deal = False


    def __init__(self, userid, otherid, is_favorite, is_deal):
        self.__userid = userid
        self.__otherid = otherid
        self.__is_favorite = is_favorite
        self.__is_deal = is_deal


    def is_favorite(self):
        return self.__is_favorite


    def is_deal(self):
        return self.__is_deal


    @classmethod
    def favorite_deal(cls, userid, otherid):
        return cls(userid, otherid, True, True)


    @classmethod
    def trash_deal(cls, userid, otherid):
        return cls(userid, otherid, False, True)


    @classmethod
    def favorite_merchant(cls, userid, otherid):
        return cls(userid, otherid, True, False)


    @classmethod
    def trash_merchant(cls, userid, otherid):
        return cls(userid, otherid, False, False)


    def as_json_for_insertion(self):
        r = {}
        key = 'favorite' if self.__is_favorite else 'trash'
        r[key] = True
        r['user_id'] = self.__userid
        key = 'deal_id' if self.__is_deal else 'merchant_id'
        r[key] = self.__otherid
        return r


    def __str__(self):
        verb = "favorited" if self.__is_favorite else "trashed"
        target_type = "deal" if self.__is_deal else "merchant"
        return "user %d %s %s %d" %  (self.__userid, verb, target_type, self.__otherid)
