#!/usr/bin/python

import elasticsearch
import json
from pprint import pprint

def index(es, json_data):
    data = json.load(json_data)
    es.index(
        index="deals",
        doc_type="all_deals",
        body={
            "uid"           : data["uid"],
            "merchantid"    : data["merchantid"],
            "img"           : data["img"],
            "description"   : data["description"],
            "inventory"     : data["inventory"],
            "max_price"     : data["max_price"],
            "min_price"     : data["min_price"]
            #category
            #sub-category
        }
    )
    #pprint(data)
    #print es.get(index="deals", doc_type="all_deals")
    #print es.search(index="deals", body={"query": {"match": {"uid": "0990"}}})

#f = open('./temp.txt')
#index(elasticsearch.Elasticsearch(), f)
#f.close()