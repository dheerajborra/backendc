#!/usr/bin/python
"""
talk to our mysql instance
TODO figure out how to persist categories for merchants
assume input has already been sanitized 
TODO is frontend handling sanitization?

@author: tomas mccandless
tdm@azul.me
"""

import MySQLdb
from core.user import *
from core.merchant import *
from core.bid import *

# we have 3 different databases:
# production, mock (use for simulation), test (use for running unittests
class DB:
    PROD = "azul_prod"
    MOCK = "azul_mock"
    TEST = "azul_test"


# a few simple lambdas for parameterized sql queries
select = lambda expr, table: "SELECT %s FROM %s;" % (expr, table)
select_w = lambda expr, table, where: select(expr, table)[:-1] + (" WHERE %s;" % where)
insert = lambda table, vals: "INSERT INTO %s VALUES %s;" % (table, str(vals))
delete = lambda table, expr: "DELETE FROM %s WHERE %s;" % (table, expr)


list_from_tuple = lambda tpls: [tpl[0] for tpl in tpls]
# "favorite_merchants" -> "merchant_id"
# "favorite_deals" -> "deal_id"
table2col_name = lambda table: table.split("_")[1][:-1] + "_id"


# get a connection object that will run a query. 
# make sure to call close() after finishing
def get_conn(use_db=DB.TEST, socket="/tmp/mysql.sock", host="128.83.196.226"):
    # TODO don't store the passwd literal in our code
    return MySQLdb.connect(host=host, user='azul', passwd='AzulRules', \
                           db=use_db, unix_socket=socket)


# take a query and a database, then execute that query
# don't call this externally
def execute_query(query, use_db=DB.TEST):
    conn = get_conn(use_db)
    c = conn.cursor()
    c.execute(query)
    rows = c.fetchall()
    c.close()
    conn.commit()
    return rows


# return the number of rows in table
def size_of_table(table, use_db=DB.TEST):
    query = select("count(1)", table)
    rows = execute_query(query, use_db)
    size = rows[0][0]
    return size


# delete everything from the table
def truncate_table(table, use_db=DB.TEST):
    query = """truncate table %s;""" % table
    execute_query(query, use_db)




"""
functions to interface with user table
"""

# take a user and put him in the database
# by default inserts in to the test database
# pass DB.MOCK or DB.PROD for other behavior
def put_user(user, use_db=DB.TEST):
    query = insert("users " + user.attrs(), user.values()[1:])
    execute_query(query, use_db)
    u = get_user_from_email(user.email(), use_db)
    return u.uid()


def get_user_from_id(uid, use_db=DB.TEST):
    query = select_w("*", "users", "id = %d" % uid)
    rows = execute_query(query, use_db)
    # assert uniqueness of id
    assert(len(rows) < 2)
    return User.from_tuple(rows[0]) if len(rows) == 1 else None


def get_user_from_email(email, use_db=DB.TEST):
    query = select_w("*", "users", "email = '%s'" % email)
    rows = execute_query(query, use_db)
    # assert uniqueness of email
    assert(len(rows) < 2)
    return User.from_tuple(rows[0]) if len(rows) == 1 else None


def get_user_from_facebook_id(facebook_id, use_db=DB.TEST):
    query = select_w("*", "users", "facebook_id = '%s'" % facebook_id)
    rows = execute_query(query, use_db)
    # assert uniqueness of facebook id 
    assert(len(rows) < 2)
    return User.from_tuple(rows[0]) if len(rows) == 1 else None



"""
functions to interface with merchant table
"""

# take a merchant and put him in the database
# by default inserts in to the test database
# pass DB.MOCK or DB.PROD for other behavior
def put_merchant(merchant, use_db=DB.TEST):
    query = insert("merchants " + merchant.attrs(), merchant.values()[1:])
    execute_query(query, use_db)
    m = get_merchant_from_email(merchant.email(), use_db)
    return m.merchant_id()


def get_merchant_from_id(mid, use_db=DB.TEST):
    query = select_w("*", "merchants", "id = %d" % mid)
    rows = execute_query(query, use_db)
    # assert uniqueness of id
    assert(len(rows) == 1)
    return Merchant.from_tuple(rows[0])


def get_merchant_from_email(email, use_db=DB.TEST):
    query = select_w("*", "merchants", "email = '%s'" % email)
    rows = execute_query(query, use_db)
    # assert uniqueness of email
    assert(len(rows) == 1)
    return Merchant.from_tuple(rows[0])




"""
functions to interface with user favorites/trash
"""

# pass in a UserMark 
# which has user id, other id, booleans for favorite/trash and deal/merchant
def add_preference_for_user(user_mark_obj, use_db=DB.TEST):
    table_prefix = "favorite" if user_mark_obj.is_favorite() else "trashed"
    table_suffix = "deals" if user_mark_obj.is_deal() else "merchants"

    table = table_prefix + "_" + table_suffix
    query = insert(table, user_mark_obj.values())
    execute_query(query, use_db)
    # now delete from the other table 
    delete_preference_for_user(user_mark_obj.flip(), use_db)
    

# pass in a UserMark
# which has user id, other id, booleans for favorite/trash and deal/merchant
def delete_preference_for_user(user_mark_obj, use_db=DB.TEST):
    table_prefix = "favorite" if user_mark_obj.is_favorite() else "trashed"
    table_suffix = "deals" if user_mark_obj.is_deal() else "merchants"
    table = table_prefix + "_" + table_suffix
    col_name = table2col_name(table) 
    expr = ("user_id=%d AND " + col_name + "=%d") % user_mark_obj.values()
    query = delete(table, expr)
    execute_query(query, use_db)


# generate and execute the proper query based on the table
# dont call this externally
def get_user_prefs(userid, table, use_db):
    # "favorite_merchants" -> "merchant_id"
    # "favorite_deals" -> "deal_id"
    db_column = table2col_name(table)
    query = select_w("distinct(%s)" % db_column, table, "user_id=%d" % userid)
    # print query
    return list_from_tuple(execute_query(query, use_db))


def get_favorite_deals_from_userid(userid, use_db=DB.TEST):
    return get_user_prefs(userid, "favorite_deals", use_db)


def get_favorite_merchants_from_userid(userid, use_db=DB.TEST):
    return get_user_prefs(userid, "favorite_merchants", use_db)


def get_trash_deals_from_userid(userid, use_db=DB.TEST):
    return get_user_prefs(userid, "trashed_deals", use_db)


def get_trash_merchants_from_userid(userid, use_db=DB.TEST):
    return get_user_prefs(userid, "trashed_merchants", use_db)




"""
functions to interface with bid_history table
"""

# pass in either object or id
obj2id = lambda obj: obj if type(obj) == type(0) else obj.uid()

def put_bid(bid_obj, use_db=DB.TEST):
    # .values() returns a tuple of all the attributes. 
    # we let database handle bid id and timestamp
    query = insert("bid_history " + bid_obj.attrs(), bid_obj.values()[1:-1])
    execute_query(query, use_db)


# pass in either a Deal object or just a deal id
# return all bids for the deal
# TODO optional parameters to filter by time
def get_bids_for_deal(deal, use_db=DB.TEST):
    dealid = obj2id(deal) 
    query = select_w("*", "bid_history", "deal_id=%d" % dealid)
    rows = execute_query(query, use_db)
    return map(Bid.from_tuple, rows)


# pass in either a User object or just a user id
# return all bids for the user
# TODO optional parameters to filter by time
def get_bids_for_user(user, use_db=DB.TEST):
    userid = obj2id(user)
    query = select_w("*", "bid_history", "user_id=%d" % userid)
    rows = execute_query(query, use_db)
    return map(Bid.from_tuple, rows)


# pass in either User object or user id,
# Deal object or deal id,
# return all the bids made for this deal and user
# TODO optional parameters to filter by time
def get_bids_for_user_and_deal(user, deal, use_db=DB.TEST):
    dealid = obj2id(deal) 
    userid = obj2id(user)
    query = select_w("*", "bid_history", "deal_id=%d AND user_id=%d" % (dealid, userid))
    rows = execute_query(query, use_db)
    return map(Bid.from_tuple, rows)
