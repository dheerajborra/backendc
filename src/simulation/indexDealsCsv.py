#!/usr/bin/python

import elasticsearch
import csv

es = elasticsearch.Elasticsearch()

f = open('../../../groupondeals.csv', 'rb')
reader = csv.reader(f)

for row in reader:
    #saving,name,price,number,value,discount,details,address,options,description
    es.index(
        index="test_all_deals",
        doc_type="test_deals",
        body={
            "saving"      :row[0].strip(),
            "name"        :row[1].strip(),
            "price"       :row[2].strip(),
            "number"      :row[3].strip(),
            "value"       :row[4].strip(),
            "discount"    :row[5].strip(),
            "details"     :row[6].strip(),
            "address"     :row[7].strip(),
            "options"     :row[8].strip(),
            "description" :row[9].strip()
            #category
            #sub-category
        }
    )

print es.get(index="test_all_deals", doc_type="deals", id=1)
#es.get("discount"='50%')
#print es.search(index="all_deals", body={"query": {"match": {"discount": "50%"}}})
f.close() 