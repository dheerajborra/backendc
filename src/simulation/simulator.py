#!/usr/bin/python

"""
run a simulation of our bidding algorithm.
create users, have them run queries and make bid requests
see what happens
"""

import csv
import random
import string
import sys
sys.path.append("./src")
from core.user import *
from core.deal import *

f = open('../CustomerPersona.csv', 'rb')
reader = csv.reader(f)

# User class: uid, firstname, lastname, sex, karma, email, password, phonenumber, birthdate
# CSV format: Name, Age, sex, persona, number of bids, max distance, UID

users = []
for row in reader:
    name = row[0].split(" ")
    firstName = name[0].capitalize()
    lastName = name[1].capitalize()
    age = row[1].strip()
    sex = row[2].strip()
    uid = int(row[6].strip())
    karma = random.randint(0, 1000)
    email = firstName + "." + lastName + "@xyz.com"
    password = ''.join(random.choice(string.lowercase) for i in range(8))
    phonenumber = ''.join(random.choice(string.digits) for i in range(10))
    birthdate = ''.join(random.choice(string.digits) for i in range(6))
    users.append(User(uid, firstName, lastName, sex, karma, email, password, phonenumber, birthdate))

for user in users:
    print user

f.close() 