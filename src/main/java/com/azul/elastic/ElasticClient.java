package com.azul.elastic;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import org.apache.log4j.Logger;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

public class ElasticClient {

	protected static final Logger logger = Logger
			.getLogger(ElasticClient.class);

	protected static Client client;

	public static void main(String args[]) throws JSONException, Exception {
		if (args.length < 2) {
			System.out
					.println("usage: java ElasticClient --get|--put <json data>");
			System.exit(1);
		}
		logger.debug("java: ElasticClient.main(" + Arrays.toString(args) + ")");
		if (args[0].equals("--get"))
			System.out.println(get(args[1]));
    else if (args[0].equals("--put"))
      System.out.println(put(args[1]));
	}

	private enum DocumentType {
		deal, query, merchant;
	}

	private static String getServerAddress() {
    // prod server
		return "128.83.196.226";
		//return "localhost";
    // dev server
    //return "128.83.52.246";
	}

	private static void setupClient() {
		client = new TransportClient()
				.addTransportAddress(new InetSocketTransportAddress(
						getServerAddress(), 9300));
	}

	public static String put(String jsonString) throws org.json.JSONException {
		if (client == null)
			setupClient();
		JSONObject jsonObject = new JSONObject(jsonString);
		return put(jsonObject);
	}

	private static String put(JSONObject jsonObject)
			throws org.json.JSONException {
		DocumentType type = DocumentType.valueOf((String) jsonObject
				.get("type"));
		String data = jsonObject.get("data").toString();
		String result = "";
		switch (type) {
		case deal:
			result = ElasticDealClient.put(data);
			break;
		case query:
			result = ElasticQueryClient.put(data);
			break;
		case merchant:
			result = ElasticMerchantClient.put(data);
			break;
		default:
			System.err.println("UNKNOWN OBJECT: " + type);
		}
		logger.trace("EC: data: " + data + " result: " + result);
		return result;
	}

	public static String get(String jsonString) throws org.json.JSONException,
			java.lang.Exception {
		logger.info("EC: get: " + jsonString);
		if (client == null)
			setupClient();
		JSONObject jsonObject = new JSONObject(jsonString);
		if(!jsonObject.has("type"))
			return "Error:: EC: invalid search object -- object doesn't have type.";
		return get(jsonObject);
	}

	private static String get(JSONObject jsonObject)
			throws org.json.JSONException, java.lang.Exception {
		DocumentType type = DocumentType.valueOf((String) jsonObject
				.get("type"));
		if(!jsonObject.has("data")){
			logger.info("EC: search object doesn't have data.");
			jsonObject.put("data", new JSONObject());
		}
		String data = jsonObject.get("data").toString();
		String result = "";
		switch (type) {
		case deal:
			result = ElasticDealClient.get(data);
			break;
		case query:
			result = ElasticQueryClient.get(data);
			break;
		case merchant:
			result = ElasticMerchantClient.get(data);
			break;
		default:
			System.err.println("UNKNOWN OBJECT: " + type);
		}
		logger.trace("EC: data: " + data + " result: " + result);
		return result;
	}

	protected static String getDistance(JSONObject fromLocation,
			JSONObject toLocation) throws org.json.JSONException {

		double lat1 = Double.parseDouble(toLocation.get("lat").toString());
		double lon1 = Double.parseDouble(toLocation.get("lon").toString());
		double lat2 = Double.parseDouble(fromLocation.get("lat").toString());
		double lon2 = Double.parseDouble(fromLocation.get("lon").toString());

		logger.trace("EC: getDistance from: " + fromLocation.toString()
				+ " getDistance to: " + toLocation.toString());

		double distance = distance(lat1, lon1, lat2, lon2, 'M');
		return String.format("%.2f", distance);
	}

	// thanks to http://www.dzone.com/snippets/distance-calculation-using-3
	private static double distance(double lat1, double lon1, double lat2,
			double lon2, char unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == 'K') {
			dist = dist * 1.609344;
		} else if (unit == 'N') {
			dist = dist * 0.8684;
		}
		return (dist);
	}

	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private static double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}

	protected static HashMap<String, Object> getTerms(JSONObject jsonObject)
			throws org.json.JSONException {
		HashMap<String, Object> terms = new HashMap<String, Object>();
		@SuppressWarnings("unchecked")
		Iterator<String> it = jsonObject.keys();
		while (it.hasNext()) {
			String key = it.next().toString();
			Object value = jsonObject.get(key);
			terms.put(key, value);
		}
		return terms;
	}

	protected static JSONObject convertToJSON(HashMap<String, Object> terms)
			throws org.json.JSONException {
		JSONObject result = new JSONObject();
		for (String term : terms.keySet()) {
			result.put(term, terms.get(term));
		}
		return result;
	}
}
