package com.azul.elastic;

import java.util.HashMap;

import org.json.JSONObject;
import org.json.JSONArray;

import org.elasticsearch.action.index.IndexResponse;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;

import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.IdsQueryBuilder;

import org.elasticsearch.search.SearchHit;

public class ElasticQueryClient extends ElasticClient {

	public static String put(String jsonString) throws org.json.JSONException {
		logger.debug("PUTTING QUERY: " + jsonString);
		IndexResponse response = client.prepareIndex("queries", "query")
				.setSource(jsonString).execute().actionGet();
		return response.getId();
	}

	public static String get(String jsonString) throws org.json.JSONException,
			java.lang.Exception {
		JSONObject jsonObject = new JSONObject(jsonString);
		return get(jsonObject);
	}

	private static String get(JSONObject jsonObject)
			throws org.json.JSONException, java.lang.Exception {
		HashMap<String, Object> searchTerms = getTerms(jsonObject);
		return get(searchTerms);
	}

	private static String get(HashMap<String, Object> searchTerms)
			throws org.json.JSONException, java.lang.Exception {

		String queryId = (String) searchTerms.get("id");
		IdsQueryBuilder qb = QueryBuilders.idsQuery().ids(queryId);

		logger.trace("qb: " + qb);

		SearchResponse response = client.prepareSearch("queries")
				.setTypes("query")
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH).setQuery(qb)
				.setFrom(0).setSize(60).setExplain(true).execute().actionGet();
		logger.trace("Query client search response: " + response);

		JSONArray results = new JSONArray();
		for (SearchHit sh : response.getHits()) {
			logger.trace("Query search hit: " + sh.sourceAsString());
			results.put(new JSONObject(sh.sourceAsString()));
		}

		JSONObject output = new JSONObject();
		output.put("query", results.optJSONObject(0));
		return output.toString();
	}

}