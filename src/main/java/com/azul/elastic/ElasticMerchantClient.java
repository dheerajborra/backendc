package com.azul.elastic;

import java.util.HashMap;

import org.json.JSONObject;
import org.json.JSONArray;

import org.elasticsearch.action.index.IndexResponse;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;

import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.GeoDistanceFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.search.SearchHit;

public class ElasticMerchantClient extends ElasticClient {

	public static String put(String jsonString) throws org.json.JSONException {
		IndexResponse response = client.prepareIndex("merchants", "merchant")
				.setSource(jsonString).execute().actionGet();
		return response.getId();
	}

	public static String get(String jsonString) throws org.json.JSONException,
			java.lang.Exception {
		JSONObject jsonObject = new JSONObject(jsonString);
		return get(jsonObject);
	}

	private static String get(JSONObject jsonObject)
			throws org.json.JSONException, java.lang.Exception {
		HashMap<String, Object> searchTerms = getTerms(jsonObject);
		return get(searchTerms);
	}

	private static String get(HashMap<String, Object> searchTerms)
			throws org.json.JSONException, java.lang.Exception {

		modifyTerms(searchTerms);

		QueryBuilder qb;
		FilterBuilder fb;
		if (searchTerms.containsKey((String) searchTerms.get("id"))) {
			String dealId = (String) searchTerms.get("id");
			qb = QueryBuilders.idsQuery().ids(dealId);
			fb = FilterBuilders.matchAllFilter();
		} else {
			qb = buildQuery(searchTerms);
			fb = buildFilter(searchTerms); // @#<$>@#$>@>

		}

		SearchResponse response = client.prepareSearch("merchants")
				.setTypes("merchant")
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH).setQuery(qb)
				.setFilter(fb) // filter based on distance
				.setFrom(0).setSize(60).setExplain(true).execute().actionGet();
		logger.trace("Merchant client search response: " + response);

		JSONObject userLocation = (JSONObject) searchTerms.get("location");
		JSONArray results = new JSONArray();
		for (SearchHit sh : response.getHits()) {
			String merchantId = sh.getId();
			JSONObject merchant = new JSONObject(sh.sourceAsString());

			merchant.put("merchant_id", merchantId);

			String[] namesList = ((String) merchant.get("name")).split("\\s+");
			StringBuilder nameBuilder = new StringBuilder();
			for (String s : namesList) {
				if (!s.equalsIgnoreCase("restaurant")) {
					nameBuilder.append(s + " ");
				}
			}
			String name = nameBuilder.toString();
			merchant.put("name", name.substring(0, name.length() - 1));

			String categories = (String) merchant.get("categories");
			String[] categoryList = categories.split("\\s+");
			merchant.put("category", categoryList[1]);

			JSONObject merchantLocation = new JSONObject(merchant.get(
					"location").toString());
			merchant.put("distance",
					getDistance(merchantLocation, userLocation));

			results.put(merchant);
		}
		return results.toString();
	}

	private static void modifyTerms(HashMap<String, Object> searchTerms) {

	}

	private static QueryBuilder buildQuery(HashMap<String, Object> searchTerms) {
		BoolQueryBuilder qb = QueryBuilders.boolQuery();

		qb.must(multiMatchQuery(
				((String) searchTerms.get("search_query")).toLowerCase(),
				"name", "categories"));

		logger.trace("search_query: "
				+ ((String) searchTerms.get("search_query")));

		logger.debug("qb: " + qb);
		return qb;
	}

	private static FilterBuilder buildFilter(HashMap<String, Object> searchTerms)
			throws org.json.JSONException {
		JSONObject searchLocation = (JSONObject) searchTerms.get("location");
		HashMap<String, Object> location = getTerms(searchLocation);

		GeoDistanceFilterBuilder fb = FilterBuilders
				.geoDistanceFilter("location")
				.point(Double.parseDouble(location.get("lat").toString()),
						Double.parseDouble(location.get("lon").toString()))
				.distance((searchTerms.get("distance") + "mi"))
				.optimizeBbox("memory");

		logger.trace("lat: "
				+ (Double.parseDouble(location.get("lat").toString())));
		logger.trace("lon: "
				+ (Double.parseDouble(location.get("lon").toString())));

		logger.debug("fb: " + fb);
		return fb;
	}
}
