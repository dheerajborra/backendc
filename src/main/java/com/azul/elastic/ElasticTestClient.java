package com.azul.elastic;

import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ElasticTestClient {

	public static void main(String[] args) throws org.json.JSONException,
			java.lang.Exception {
		for (int i = 0; i < args.length; ++i) {
			String flag = args[i];

			if (flag.equals("--putDeals"))
				testPutDeals();
			else if (flag.equals("--putMerchants"))
				testPutOtherMerchants();
			else if (flag.equals("--getDeals"))
				testGetDeals();
			else if (flag.equals("--getQuery"))
				testGetQuery(args[++i]);
			else if (flag.equals("--getOtherMerchants"))
				testGetOtherMerchants();
			else if (flag.equals("--populate")) {
				String indexType = args[++i];
				String fileName = args[++i];
				populate(fileName, indexType);
			}
		}
	}

	private static void testPutDeals() throws org.json.JSONException,
			java.lang.Exception {
		// location is ["lon", "lat"]
		String[] categories = { "Mexican", "Italian", "Indian", "French" };
		Random rand = new Random();
		for (int i = 0; i < 30; ++i)
			ElasticClient
					.put("{ \"data\" : { \"name\": \"Half off Pizza\","
							+ "\"address\": \""
							+ rand.nextInt(100)
							+ " Research Blvd Ste\","
							+ "\"categories\" : \""
							+ categories[rand.nextInt(4)]
							+ "\","
							+ "\"reg_price\" : "
							+ ((((Math.random() * 10) % 3) + 1) * 10)
							+ ","
							+ "\"min_price\" : 3,"
							+ "\"max_price\" : 10,"
							+ "\"start_date\" : \"2013-07-28 08:30:00\","
							+ "\"end_date\" : \"2013-08-28 08:30:00\","
							+ "\"days\" : [\"0\",\"2\",\"5\"],"
							+ "\"location\" : {\"lat\": 30.7809035011582, \"lon\": -97.7809035011582},"
							+ "\"max_per_user\" : 5,"
							+ "\"description\" : \"Taste this yummy pizza straight from the brass oven\", "
							+ "\"fine_print\": \"Damn you fine\","
							+ "\"bid_type\" : 1,"
							+ "\"city\" : \"Austin\","
							+ "\"redemption_start_time\" : \"08:30:00\","
							+ "\"redemption_end_time\" : \"22:30:00\","
							+ "\"redemption_expiration\" : \"2013-08-28 08:30:00\","
							+ "\"img\" : \"sdfsad\","
							+ "\"num_deals\": 1000,"
							+ "\"website\": \"http://www.drunkfishusa.net\","
							+ "\"zip\": 78759,"
							+ "\"merchant_id\": 1 },"
							+ "\"type\" : \"deal\" }");
	}

	private static void testPutOtherMerchants() throws org.json.JSONException,
			java.lang.Exception {
		String[] categories = { "Mexican", "Italian", "Indian", "French" };
		Random rand = new Random();
		for (int i = 0; i < 30; ++i)
			// location is ["lon", "lat"]
			ElasticClient
					.put("{ \"data\" : { \"name\": \"Dick's Kitchen\","
							+ "\"categories\" : \""
							+ categories[rand.nextInt(4)]
							+ "\","
							+ "\"rating\" : \""
							+ (rand.nextDouble() % 5)
							+ "\","
							+ "\"location\" : {\"lat\": 30.7809035011582, \"lon\": -97.7809035011582},"
							+ "\"price_range\": \"$$\" },"
							+ "\"type\" : \"merchant\" }");
	}

	private static void testGetDeals() throws org.json.JSONException,
			java.lang.Exception {
		String results = ElasticClient.get("{ \"data\" : {\"distance\": 10, "
				+ "\"date_time\": \"2013-08-27T08:34:54\","
				+ "\"location\": {\"lat\": 30.279739, \"lon\": -97.7472669},"
				+ "\"search_query\": \"\"," + "\"user_id\" : 1,"
				+ "\"price\":50.0 }, " + "\"type\" : \"deal\" }");

		// String results = ElasticClient
		// .get("{ \"data\" : {\"id\": \"qsmiLnAsQ42ftbmCvQ4LKw\", \"decrement\" : true }, "
		// + "\"type\" : \"deal\" }");

		System.out.println(results);
	}

	private static void testGetQuery(String id) throws org.json.JSONException,
			java.lang.Exception {
		String results = ElasticClient
				.get("{ \"data\" : {\"id\": \"" + id +"\"}, "
						+ "\"type\" : \"query\" }");

		System.out.println(results);
	}

	private static void testGetOtherMerchants() {

	}

	private static void populate(String fileName, String type)
			throws IOException, ParseException, JSONException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(fileName));
		JSONArray instances = (JSONArray) obj;
		for (int i = 0; i < instances.size(); ++i) {
			JSONObject instance = (JSONObject) instances.get(i);
			ElasticClient.put("{ \"data\" : " + instance.toJSONString() + ","
					+ "\"type\" : \"" + type + "\" }");
		}
	}
}
