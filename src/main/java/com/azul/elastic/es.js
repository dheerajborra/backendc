// node-java is installed on 77
var java = require("java");

// how do you pass query_object (JSON) ?
var query_object = ?

// To get deals in the format as mentioned in end points doc
java.callStaticMethod("com.azul.elastic.ElasticClient", "get_deals", query_object, function(err, results) {
  if(err) { console.error(err); return; }
  // 'results' from get_deals
  
  // get_deals return a json object with deal data
  // it doesn't have this info yet: "address", "state", "website", "zip", "distance", "now_price", "rating"
  // needed from bidding algo? 
});

// To put_json_object (deal, query) in the format as mentioned in end points doc
java.callStaticMethod("com.azul.elastic.ElasticClient", "put_json_object", query_object, function(err, results) {
  if(err) { console.error(err); return; }
  // 'results' from get_deals
  
  // get_deals return a json object with deal data
  // it doesn't have this info yet: "address", "state", "website", "zip", "distance", "now_price", "rating"
  // needed from bidding algo? 
});