package com.azul.elastic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import org.elasticsearch.action.index.IndexResponse;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;

import static org.elasticsearch.index.query.FilterBuilders.*;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.BoolFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.search.SearchHit;

public class ElasticDealClient extends ElasticClient {

	public static String put(String deal) throws org.json.JSONException {
		IndexResponse response = client.prepareIndex("deals", "deal")
				.setSource(deal).execute().actionGet();
		return response.getId();
	}

	private static void update(String dealId) throws JSONException, Exception {
		QueryBuilder qb = QueryBuilders.idsQuery().ids(dealId);
		FilterBuilder fb = FilterBuilders.matchAllFilter();
		SearchResponse response = get(qb, fb);

		// must exist
		SearchHit sh = response.getHits().getAt(0);
		logger.trace("EDC: Deal to update: " + sh.sourceAsString());

		JSONObject deal = new JSONObject(sh.sourceAsString());

		int inventory = (Integer) deal.get("num_deals");
		if (inventory > 0) {
			Map<String, Object> updateObject = new HashMap<String, Object>();
			updateObject.put("num_deals", --inventory);
			client.prepareUpdate("deals", "deal", dealId)
					.setScript("ctx._source.num_deals=num_deals")
					.setScriptParams(updateObject).execute().actionGet();
		}
	}

	public static String get(String searchString)
			throws org.json.JSONException, java.lang.Exception {
		logger.debug("EDC: Search String: " + searchString);
		JSONObject searchObject = new JSONObject(searchString);
		return get(searchObject);
	}

	private static String get(JSONObject searchObject)
			throws org.json.JSONException, java.lang.Exception {
		HashMap<String, Object> searchTerms = getTerms(searchObject);
		return get(searchTerms);
	}

	private static String get(HashMap<String, Object> searchTerms)
			throws org.json.JSONException, java.lang.Exception {

		if (searchTerms.containsKey("id")) {
			return getById(searchTerms);
		} else {
			modifyTerms(searchTerms);
			return getBySearch(searchTerms);
		}
	}

	private static void modifyTerms(HashMap<String, Object> searchTerms) {
		if (!searchTerms.containsKey("search_query")
				|| ((String) searchTerms.get("search_query")).equals("")) {
			logger.info("EDC: search_query in data is empty: adding a default query.");
			searchTerms.put("search_query", "restaurant");
		}
		String searchQuery = ((String) searchTerms.get("search_query"))
				.toLowerCase();
		String[] searchQueryTerms = searchQuery.split("\\s+");
		StringBuilder builder = new StringBuilder();
		for (String s : searchQueryTerms) {
			if (s.equals("lunch") || s.equals("dinner") || s.equals("food")
					|| s.equals("nom"))
				builder.append("restaurant");
			else
				builder.append(s);
		}
		searchTerms.put("search_query", builder.toString());
	}

	private static String getBySearch(HashMap<String, Object> searchTerms)
			throws Exception {
		if (!searchTerms.containsKey("location")) {
			logger.info("EDC: location in data is empty: adding default location: UT Austin");
			JSONObject location = new JSONObject();
			location.put("lat", 30.2861);
			location.put("lon", -97.7394);
			searchTerms.put("location", location);
		}
		if (!searchTerms.containsKey("distance")) {
			logger.info("EDC: distance in data is empty: adding default distance: 2");
			searchTerms.put("distance", 2);
		}
		if (!searchTerms.containsKey("price")) {
			logger.info("EDC: price in data is empty: adding default price: 10");
			searchTerms.put("price", 10);
		}
		if (!searchTerms.containsKey("date_time")
				|| ((String) searchTerms.get("date_time")).equals("")) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.MINUTE, 10);
			String dateTime = dateFormat.format(cal.getTime());
			logger.info("EDC: data_time in data is empty: adding date_time: "
					+ dateTime);
			searchTerms.put("date_time", dateTime);
		}
		if (!searchTerms.containsKey("user_id")) {
			int userId = (new Random()).nextInt(1000) + 1000;
			logger.info("EDC: user_id in data is empty: adding user_id: "
					+ userId);
			searchTerms.put("user_id", userId);
		}

		QueryBuilder qb = buildQueryForSearch(searchTerms);
		FilterBuilder fb = buildFilterForSearch(searchTerms);
		SearchResponse response = get(qb, fb);

		JSONObject userLocation = (JSONObject) searchTerms.get("location");

		JSONArray searchResults = new JSONArray();
		for (SearchHit sh : response.getHits()) {
			logger.trace("EDC: Deal search hit: " + sh.sourceAsString());

			String dealId = sh.getId();
			JSONObject deal = new JSONObject(sh.sourceAsString());
			if ((Integer) deal.get("num_deals") == 0)
				continue;

			JSONObject result = parseDeal(deal, dealId, userLocation);

			// USED for BIDDING
			result.put("merchant_id", deal.get("merchant_id"));
			result.put("max_price", deal.get("max_price"));
			result.put("min_price", deal.get("min_price"));
			result.put("bid_window", searchTerms.get("date_time").toString()
					.replace('T', ' '));

			searchResults.put(result);
		}

		// get the relevant merchants we dont have deals with
		JSONObject otherMerchantSearchObject = new JSONObject();
		otherMerchantSearchObject.put("data", convertToJSON(searchTerms));
		otherMerchantSearchObject.put("type", "merchant");
		String otherMerchants = ElasticClient.get(otherMerchantSearchObject
				.toString());

		// put the deals into the query object
		JSONObject query = new JSONObject();
		query.put("deals", relevantDataForQueryIndex(searchResults));
		query.put("user_id", searchTerms.get("user_id"));

		// put the query back into elasticsearch
		JSONObject queryObject = new JSONObject();
		queryObject.put("type", "query");
		queryObject.put("data", query);
		String queryId = ElasticClient.put(queryObject.toString());

		// return the full query object
		JSONObject output = new JSONObject();
		output.put("query_id", queryId);
		output.put("query", query);
		output.put("results", searchResults);
		output.put("others", new JSONArray(otherMerchants));
		return output.toString();
	}

	private static String getById(HashMap<String, Object> searchTerms)
			throws Exception {

		String dealId = (String) searchTerms.get("id");
		if (!searchTerms.containsKey("decrement")) {
			logger.info("EDC: decrement in data is empty: adding false");
			searchTerms.put("decrement", false);
		}
		if ((Boolean) searchTerms.get("decrement")) {
			update(dealId);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}

		QueryBuilder qb = QueryBuilders.idsQuery().ids(dealId);
		FilterBuilder fb = FilterBuilders.matchAllFilter();
		SearchResponse response = get(qb, fb);

		SearchHit sh = response.getHits().getAt(0);
		logger.trace("EDC: DealId hit: " + sh.sourceAsString());

		JSONObject deal = new JSONObject(sh.sourceAsString());

		JSONObject result = parseDeal(deal, dealId,
				(JSONObject) deal.getJSONObject("location"));

		result.put("merchant_id", deal.get("merchant_id"));

		return result.toString();
	}

	private static SearchResponse get(QueryBuilder qb, FilterBuilder fb)
			throws org.json.JSONException, java.lang.Exception {
		SearchResponse response = client.prepareSearch("deals")
				.setTypes("deal")
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH).setQuery(qb)
				.setFilter(fb).setFrom(0).setSize(60).setExplain(true)
				.execute().actionGet();
		logger.trace("EDC: Deal client search response: " + response);
		return response;
	}

	private static JSONObject parseDeal(JSONObject deal, String dealId,
			JSONObject userLocation) throws Exception {
		JSONObject result = new JSONObject();

		result.put("deal_id", dealId);

		String categories = (String) deal.get("categories");
		String[] categoryList = categories.split("\\s+");
		result.put("category", categoryList[1]);
		// FIX THIS: tag with what?
		result.put("img", deal.get("img"));

		result.put("description", deal.get("description"));

		result.put("favorite", false);

		result.put("fineprint", deal.get("fine_print"));

		result.put("price_reg", deal.get("reg_price"));

		// FIX THIS: Decrement when a purchase is done
		result.put("inventory", deal.get("num_deals"));

		Integer maxPrice = (Integer) deal.get("max_price");
		result.put("price_now", maxPrice);
		result.put("price_prev", maxPrice);

		int merchantId = (Integer) deal.get("merchant_id");
		JSONObject merchantInfo = getMerchantInformation(merchantId);
		JSONObject merchant = (JSONObject) merchantInfo.get("merchant");
		result.put("merchant", merchant.get("name"));
		result.put("rating", merchant.get("rating"));
		result.put("address", merchant.get("address"));

		JSONObject dealLocation = (JSONObject) deal.get("location");
		String distance = getDistance(dealLocation, userLocation);
		result.put("location", dealLocation);
		result.put("distance", distance);

		return result;
	}

	private static QueryBuilder buildQueryForSearch(
			HashMap<String, Object> searchTerms) {
		BoolQueryBuilder qb = QueryBuilders.boolQuery();

		qb.must(rangeQuery("reg_price").from(0).to(
				Double.parseDouble(searchTerms.get("price").toString()) + 5));

		qb.must(multiMatchQuery(
				((String) searchTerms.get("search_query")).toLowerCase(),
				"name", "categories", "description").lenient(true));

		logger.trace("EDC: Deal price: "
				+ (Double.parseDouble(searchTerms.get("price").toString())));
		logger.trace("EDC: Deal query: "
				+ ((String) searchTerms.get("search_query")));

		// logger.trace("EDC: qb: " + qb);
		return qb;
	}

	private static FilterBuilder buildFilterForSearch(
			HashMap<String, Object> searchTerms) throws org.json.JSONException {
		BoolFilterBuilder fb = FilterBuilders.boolFilter();

		JSONObject locationObject = (JSONObject) searchTerms.get("location");
		HashMap<String, Object> location = getTerms(locationObject);
		fb.must(geoDistanceFilter("location")
				.point(Double.parseDouble(location.get("lat").toString()),
						Double.parseDouble(location.get("lon").toString()))
				.distance((searchTerms.get("distance") + "mi"))
				.optimizeBbox("memory"));

		// fb.must(rangeFilter("redemption_expiration").from(
		// (String) searchTerms.get("date_time").toString()
		// .replace('T', ' ')));

		logger.trace("EDC: Deal around lat : "
				+ (Double.parseDouble(location.get("lat").toString())));
		logger.trace("EDC: Deal around lon : "
				+ (Double.parseDouble(location.get("lon").toString())));
		logger.trace("EDC: Deal around dist: " + searchTerms.get("distance")
				+ "mi");
		logger.trace("EDC: redemption expiration: "
				+ ((String) searchTerms.get("date_time").toString()
						.replace('T', ' ')));

		// logger.trace("EDC: fb: " + fb);
		return fb;
	}

	private static JSONArray relevantDataForQueryIndex(JSONArray results)
			throws org.json.JSONException {
		JSONArray relevantInformation = new JSONArray();

		// deal_id, merchant_id, max_price, min_price, inventory, bid_window,
		// last_bid_time
		String[] info = { "deal_id", "max_price", "min_price", "merchant_id",
				"inventory", "bid_window" };
		for (int j = 0; j < results.length(); ++j) {
			JSONObject result = results.getJSONObject(j);
			JSONObject revisedResult = new JSONObject();
			revisedResult.put("last_bid_time", "1990-01-01 00:00:00");
			for (String i : info) {
				revisedResult.put(i, result.get(i));
			}
			relevantInformation.put(revisedResult);
		}
		return relevantInformation;
	}

	// 2048 is for unit testing
	// 2049 is for simulation
	// 2050 is for production
	private static final String url = "http://128.83.196.226:2049/merchants/";

	private static JSONObject getMerchantInformation(int merchantId)
			throws Exception {
		URL URLObject = new URL(url + merchantId);
		HttpURLConnection connection = (HttpURLConnection) URLObject
				.openConnection();

		connection.setRequestMethod("GET");

		int responseCode = connection.getResponseCode();
		logger.trace("EDC: Sending 'GET' request to URL : " + url);
		logger.trace("EDC: Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		logger.trace("EDC: getMerchantInformation RESPONSE: "
				+ response.toString());

		return new JSONObject(response.toString());
	}
}
