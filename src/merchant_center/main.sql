# Returns table telling how much of each grouping counted
# action: 
#	-1: bid down
#	-2: bit up
#	1:	purchase
# Age:
#	-1: undisclosed
# Sex:
#	U: undisclosed
# 	'': not sure what this means, may be old data

# WILL WORK AS IT SELF
# for some modification simply change the time 
# stamps to be variables which are passed in
# Suggestion: add age groupings as we can make multiple calls to grab required groupings
SELECT COUNT(id) as 'count'
	,action
	,age
	,sex
FROM (
	SELECT
		users.id
		,actions.deal_id
		,actions.price
		,actions.action
		,IF(users.sex IS NULL, 'U', users.sex) as 'sex'
		,IF(users.age IS NULL, -1, users.age) as 'age'
	FROM 
		(
			(
				## get relavant bids
				SELECT 
					user_id,
					deal_id,
					new_price as 'price',
					IF(new_price < old_price, -1, -2) as action
				FROM bid_history 
				WHERE 
					time BETWEEN FROM_UNIXTIME(1385162044) AND FROM_UNIXTIME(UNIX_TIMESTAMP())
					-- AND query_id IN("queryid")

			) UNION (
				## get relavant purchases
				SELECT 
					user_id,
					deal_id,
					price_now as 'price',
					1 as action
				FROM purchases 
				WHERE 
					purchase_time BETWEEN FROM_UNIXTIME(1385162044) AND FROM_UNIXTIME(UNIX_TIMESTAMP())
					AND deal_id NOT IN("deal_id")
			)
		) as actions
	LEFT JOIN
		(
			SELECT
				id,
				FLOOR(DATEDIFF(CURDATE(),birthdate)/365.25) as 'age',
				sex
			FROM 
				users
		) as users
	ON actions.user_id = users.id
) data
GROUP BY action, sex, age
;


SELECT
    SUM(IF(age < 18,1,0)) as 'Under 18',
    SUM(IF(age BETWEEN 18 and 25,1,0)) as '18 - 25',
    SUM(IF(age BETWEEN 25 and 35,1,0)) as '25 - 35',
    SUM(IF(age BETWEEN 35 and 45,1,0)) as '35 - 45',
    SUM(IF(age BETWEEN 45 and 60,1,0)) as '45 - 60',
    SUM(IF(age >=60, 1, 0)) as 'Over 60',
    SUM(IF(age IS NULL, 1, 0)) as 'Not Filled In (NULL)'

FROM (SELECT TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) AS age FROM users) as derived;
