#!/usr/bin/python

"""
flask server that handles bidding.
upon receving a post request with a query id,
fork off a new process that handles bidding

@author tomas mccandless
tdm@azul.me
"""

import threading
import Queue
import os
import signal
import sys
sys.path.append('../core')

from flask import Flask, request
from query import *
from bid_processor import *
from time import time


app = Flask(__name__)

# track which pids are bidding for each user
user_bidding_pids = {}
# track which user a particular pid is bidding for
pid_bidding_users = {}

@app.route('/bid/<query_id>', methods=['POST'])
def bid(query_id):
    json_data = request.get_json(force=True)
    #print 'flask: incoming json: %s ' % json_data

    try:
        q = Query(json_data)
    except:
        print "flask: error constructing query object"
        return 'flask: invalid json for Query %s' % query_id

    user_id = q.user_id()
    print 'flask: bid request: user_id: %s, query_id: %s' % (user_id, query_id)

    # if the user isnt logged in, just log the request and don't start bidding
    if user_id == '' or user_id == None or int(user_id) < 0:
        s = 'flask: bidding not started for user %s' % user_id
        print s
        return s

    bid_time_remaining = q.time_remaining()

    # set a bid window of 10 minutes if what we receive is too soon in the future
    if bid_time_remaining < 60:
        bid_time_remaining = 600

    bid_queue.put((time() + 5, q, bid_time_remaining))
    return ('flask: added query %s for user %s to queue.' % (query_id, user_id))



# forked bidding processes make a request to this endpoint just before they terminate
# now deprecated with move to threadpool bidding architecture
@app.route('/bidding_expiry/<pid>', methods=['POST'])
def bidding_expired(pid):
    try:
        pid = int(str(pid))
    except:
        s = 'flask: invalid pid %s' % str(pid)
        print s
        return s

    user_id = pid_bidding_users[pid]
    print 'flask: received bidding expired event from pid %d' % pid
    del user_bidding_pids[user_id]
    del pid_bidding_users[pid]
    return 'flask: deleted entry for pid %d' % pid


if __name__ == '__main__':
    bid_queue = Queue.PriorityQueue(0)
    BID_PROCESSORS = 16

    for i in xrange(BID_PROCESSORS):
        print 'starting worker', i
        BidProcessor(bid_queue).start()


    print 'flask: starting bid server'
    app.debug = False
    app.run(host='0.0.0.0')
