all:
	make test_no_db
	python test/test_mysql.py

test_no_db:
	python test/test_user.py
	python test/test_merchant.py
	python test/test_deal.py
	python test/test_bid.py
	python test/test_usermark.py
	python test/test_bid_perform.py

test_user:
	python test/test_user.py

test_merchant:
	python test/test_merchant.py

test_deal:
	python test/test_deal.py

test_bid:
	python test/test_bid.py

test_bid_perform:
	python test/test_bid_perform.py

test_usermark:
	python test/test_usermark.py

test_mysql:
	python test/test_mysql.py

test_node_mysql:
	python test/test_node_mysql.py

test_es:
	mvn compile
	mvn exec:java -Dexec.mainClass="com.azul.elastic.ElasticClient"

node:
	mvn clean compile assembly:single
	supervisor ./api-server/api-server.js
