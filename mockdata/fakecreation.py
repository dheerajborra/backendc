import json
import pickle
import random


#Fake object contains 

fake_object = []
categories = ["Restaurants", "Home and Auto", "Events", "Fitness", "Beauty"]

def getdata(filename):
    json_data = open(filename)
    json_array = json.load(json_data)
    return json_array

def main():
    createobject("beauty.json")
    createobject("events.json")
    createobject("fitness.json")
    createobject("home.json")
    createobject("health.json")
    createobject("restaurants.json")
    f = open("mockdata.json" ,"r+")
    json.dump(fake_object,f)
    
def createobject(filename):
    array = getdata(filename)
    for item in array[:20]:
        fake_dict = {}
        if "name" in item.keys():
            fake_dict["name"] = item["name"].strip()
        if "price" in item.keys():
            fake_dict["priceMax"] = str(int(float(item["value"].replace("$","").replace(",",""))*100))
        if "value" in item.keys():
            fake_dict["priceMin"] = str(int(float(item["price"].replace("$","").replace(",",""))*100))
        fake_dict["remaining"] = random.randint(1,100)
        random.shuffle(categories)
        fake_dict["id"] = str(random.randint(10000,99999))
        fake_dict["categories"] = ", ".join(categories[:random.randint(1,5)])
        fake_dict["lat"] = random.uniform(30.2,30.3)
        fake_dict["lon"] = random.uniform(-97.7,-97.8)
        fake_dict["bidType"] = random.randint(0,2)
        startday = random.randint(10,29)
        endday = random.randint(startday+1,30)
        fake_dict["startDate"] = "2013-08-"+str(startday)+"T10:20:30.45+01:00"
        fake_dict["endDate"] = "2013-08-"+str(endday)+"T23:20:30.45+01:00"
        fake_object.append(fake_dict)

