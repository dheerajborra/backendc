import urllib
import urllib2
import json
import sys
import os
import random

"""

"bid_type", "address", "categories", "city", "days", "description", "fine_print", "img", "location", 
"max_per_user", "max_price", "merchant_id", "min_price", "name", "num_deals", "redemption_end_time", 
"redemption_start_time", "redemption_expiration", "reg_price", "start_date", "end_date", "website", 
"state", "zip"

u'website': u'http://www.noodles.com/locations/9\u2026', 
u'rating': u'3.0',
u'endDate': u'2013-09-26T21:00:00', 
u'name': u'Noodles & Company', 
u'zip': u'78757', 
u'city': u'Austin', 
u'number': u'(512) 420-0508', 
u'state': u'TX', 
u'priceMax': 1000, 
u'address': u'2525 W Anderson', 
u'startDate': u'2013-08-23T11:00:00', 
u'priceMin': 100, 
u'categories': u'Restaurants Mediterranean Restaurants American (Traditional) Restaurants Asian Fusion'

"""

jsonData = open('./../raw/deals2.json')
data = json.load(jsonData)
outFile  = open('./../formatted/deals2.json', 'w')

for x in xrange(0, len(data)):
    try:
        # print "DEAL: ", data[x]
        address = data[x]["address"]+ " " + data[x]["city"] + " " + data[x]["zip"]
        address = address.replace(" ", "+")
        
        print "MAKING LOCATION REQUEST for ", address, "..."
        url = "http://nominatim.openstreetmap.org/search?q=%s&format=json&polygon=0&polygon_geojson=1&addressdetails=1" % address
        response = urllib2.urlopen(url)
        jsonGeoCode = response.read()
        reply = json.loads(jsonGeoCode)

        if len(reply) == 0:
            continue
        lat = float(reply[0].get("lat"))
        if lat == None:
            continue
        lon = float(reply[0]["lon"])

        print "PUTTING INFO..."
        name = data[x]["name"]

        description = data[x]["name"]

        city =  data[x]["city"]

        state = reply[0]["address"]["state"]

        zipCode = int(data[x]["zip"])

        reg_price = 0.0
        try:
            pm = str(data[x]["priceMax"])
            reg_price = float(pm[:-2] + "." + pm[-2:])
        except KeyError:
            reg_price = random.randint(10, 100)

        max_price = 0.0
        try:
            pm = str(data[x]["priceMin"])
            max_price = (float(pm[:-2] + "." + pm[-2:]) + reg_price)/2
        except KeyError:
            max_price = reg_price * random.random()

        min_price = float(max_price * random.random())

        print "PRICE MIN: ", min_price, " MAX: ", max_price

        bid_type = random.randint(0, 2)

        categories = data[x]["categories"]
        if (isinstance(data[x]["categories"], list)):
            categories = " ".join(data[x]["categories"])
        categories = categories      

        days = ["0", "1"]

        fine_print = "(c) 2013 Azul Mobile Inc."

        max_per_user = random.randint(1,10)

        phonenumber = data[x]["number"]
        if (isinstance(data[x]["number"], list)):
            phonenumber = data[x]["number"][0]

        print "MAKING MERCHANT REQUEST..."
        email = phonenumber + "@qaz.me"
        newMerchantData = {"name": name, "address": address.replace("+", " "), "latitude": lat, "longitude": lon, 
        "phonenumber" : phonenumber,  "email": email, "rating" : float(data[x]["rating"])}
        newMerchant = urllib.urlencode(newMerchantData)
        # make POST requests
        url = "http://128.83.196.226:2048/merchants/"
        req = urllib2.Request(url, newMerchant)
        response = urllib2.urlopen(req)
        merchantCode = response.read()
        reply = json.loads(merchantCode)
        merchant_id = reply.get("id")
        if ( not isinstance(merchant_id, int)):
            url = "http://128.83.196.226:2048/merchants/?%s" % urllib.urlencode({"email": email})
            print "MERCHANT EXISTS, making request to: ", url
            response = urllib.urlopen(url)
            merchantCode = response.read()
            reply = json.loads(merchantCode)
            merchant_id = reply["merchant"]["id"]
        
        if (not isinstance(merchant_id, int)):
            print "merchant_id is null"
            continue
            
        print "ADDING MORE INFO..."
        num_deals = random.randint(1,10) * 10

        redemption_end_time =  "08:30:00"

        redemption_start_time = "22:30:00"

        expday = str(random.randint(10, 29))
        redemption_expiration = "2014-12-"+ expday +" 23:59:59"

        startday = str(random.randint(10, 29))
        start_date = "2013-08-"+ startday +" 08:30:00"

        endday = str(random.randint(1, 30))
        end_date = "2014-01-"+ endday + " 22:30:00"

        website = "www.google.com"

        img = "img"

        json.dump({"bid_type": bid_type, "address" : address.replace("+", " "), "categories" :categories, "city" :city, "days" :days, 
            "description" :description, "fine_print": fine_print, "img" : img, "location": {"lat": lat, "lon" : lon}, 
            "max_per_user" : max_per_user, "max_price" : max_price, "merchant_id" : merchant_id, 
            "min_price" : min_price, "name" : name, "num_deals" : num_deals, "redemption_end_time" : redemption_end_time, 
            "redemption_start_time" : redemption_start_time, "redemption_expiration" : redemption_expiration, 
            "reg_price" : reg_price, "start_date" : start_date, "end_date" : end_date, "website" : website, 
            "state" : state, "zip" : zipCode }, outFile)
        print "...ADDED DEAL."
        outFile.write("\n")
    except Exception, e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print "FAILED", e, exc_type, fname, exc_tb.tb_lineno
        continue

jsonData.close()
outFile.close()