import urllib2
import urllib
import json
import sys
import random
import csv

outFile  = open('./../formatted/deals.json', 'w')

"""
"bid_type", "address", "categories", "city", "days", "description", "fine_print", "img", "location", 
"max_per_user", "max_price", "merchant_id", "min_price", "name", "num_deals", "redemption_end_time", 
"redemption_start_time", "redemption_expiration", "reg_price", "start_date", "end_date", "website", 
"state", "zip"
"""

with open("./../raw/deals.csv", 'rb') as f:
    reader = csv.DictReader(f)
    dics = [ d for d in reader ]

    # name,address,city,state,zip,latitude,longitude,max_price,min_price,reg_price,num_deals,website,
    # img,description,categories,rating,phonenumber,email
    for instance in dics:
        
        address = instance["address"]
        name = instance["name"]
        lat = float(instance["latitude"])
        lon = float(instance["longitude"])
        email = instance["email"]
        phonenumber = instance["phonenumber"]
        rating = instance["rating"]
        instance["reg_price"] = float(instance["reg_price"])
        instance["min_price"] = float(instance["min_price"])
        instance["max_price"] = float(instance["max_price"])
        instance["zip"] = int(instance["zip"])
        instance["rating"] = float(instance["rating"])
        instance["num_deals"] = int(instance["num_deals"])

        # url = "http://128.83.196.226:2049/merchants/?%s" % urllib.urlencode({"email": email})
        # response = urllib.urlopen(url)
        # merchantCode = response.read()
        # reply = json.loads(merchantCode)
        # merchant_id = reply["merchant"]["id"]

        newMerchantData = {"name": name, "address": address, "latitude": lat, "longitude": lon, 
        "phonenumber" : phonenumber,  "email": email, "rating" : rating }
        newMerchant = urllib.urlencode(newMerchantData)
        # make POST requests
        url = "http://128.83.196.226:2049/merchants/"
        req = urllib2.Request(url, newMerchant)
        response = urllib2.urlopen(req)
        merchantCode = response.read()
        reply = json.loads(merchantCode)
        merchant_id = reply.get("id")

        instance.pop("latitude", None)
        instance.pop("longitude", None)
        instance.pop("email", None)
        instance.pop("phonenumber", None) 
        instance.pop("rating", None) 
        
        instance["merchant_id"] = merchant_id    
        instance["location"] = {"lat": lat, "lon" : lon}

        instance["bid_type"] = random.randint(0, 2)
        instance["days"] = ["0", "1"]
        instance["fine_print"] = "(c) 2013 Azul Mobile Inc."
        instance["max_per_user"] = 1
        instance["redemption_end_time"] =  "08:30:00"
        instance["redemption_start_time"] = "22:30:00"

        expday = str(random.randint(10, 29))
        instance["redemption_expiration"] = "2014-12-"+ expday +" 23:59:59"

        startday = str(random.randint(10, 29))
        instance["start_date"] = "2013-08-"+ startday +" 08:30:00"

        endday = str(random.randint(1, 30))
        instance["end_date"] = "2014-01-"+ endday + " 22:30:00"

        # print "INSTANCE: ", instance
        json.dump(instance, outFile)
        outFile.write("\n")

outFile.close()