import urllib2
import json
import sys
import os

jsonData = open('./../raw/otherMerchants.json')
data = json.load(jsonData)
outFile  = open('./../formatted/otherMerchants.json', 'w')

for x in xrange(0, len(data)):
    try:
        address = data[x]["address"]
        address = address.replace(" ", "+")

        print "MAKING LOCATION REQUEST for ", address, "..."          
        url = "http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false" % address
        response = urllib2.urlopen(url)
        jsonGeoCode = response.read()
        print len(jsonGeoCode), " r: ", jsonGeoCode
        reply = json.loads(jsonGeoCode)
        lat = reply["results"][0]["geometry"]["location"]["lat"]
        lon = reply["results"][0]["geometry"]["location"]["lng"]

        # url = "http://nominatim.openstreetmap.org/search?q=%s&format=json&polygon=0&polygon_geojson=1&addressdetails=1" % address
        # response = urllib2.urlopen(url)
        # jsonGeoCode = response.read()
        # #print len(jsonGeoCode), " r: ", jsonGeoCode
        # reply = json.loads(jsonGeoCode)
        # #print "reply: ", reply
        # if len(reply) == 0:
        #     continue
        # lat = float(reply[0].get("lat"))
        # if lat == None:
        #     continue
        # lon = float(reply[0]["lon"])
        
        print "PUTTING INFO..."
        r = data[x].get("rating")
        rating = r if r != None else "0.0"
        
        categories = data[x]["categories"]
        
        name = data[x]["name"]
        
        pr = data[x].get("price_range")
        priceRange = pr if pr != None else "$$"
        
        json.dump({ "location": {"lat": lat, "lon" : lon}, "rating": rating, "name": name, "categories": categories, "price_range":  priceRange}, outFile)
        print "...ADDED MERCHANT." 
        outFile.write("\n")
    except Exception, e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print "FAILED", e, exc_type, fname, exc_tb.tb_lineno
        continue

jsonData.close()
outFile.close()