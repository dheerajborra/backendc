import urllib2
import urllib
import json
import sys
import random

"""

"bid_type", "address", "categories", "city", "days", "description", "fine_print", "img", "location", 
"max_per_user", "max_price", "merchant_id", "min_price", "name", "num_deals", "redemption_end_time", 
"redemption_start_time", "redemption_expiration", "reg_price", "start_date", "end_date", "website", 
"state", "zip"

"phone_number": "512-268-3697 | 830-627-3697", 
"saving": "$10", 
"description": "$10 for $20 Worth of Dine-In or Take-Out Pizza at Fox's Pizza Den", "
price": "$10", 
"value": "$20", 
"discount": "50%", 
"details": "Expires 180 days after purchase. Limit 1 per person, may buy 1 additional as a gift. Limit 1 per table. Limit 1 per visit. Valid only for option purchased. Not valid for alcohol. Not valid for delivery. Must use promotional value in 1 visit. Not valid for online orders. Merchant is solely responsible to purchasers for the care and quality of the advertised goods and services. that apply to all deals.", 
"address": "147 Elmhurst Dr Kyle, Texas 78640 | 1551 N Walnut Ave New Braunfels, Texas 78130", 
"options": "Choose Between Two Options $10 for $20 worth of dine-in pizza $10 for $20 worth of take-out pizza See the"

name, address, latitude, longitude, phonenumber, email, rating, price, flags

"""

jsonData = open('./../raw/deals.json')
data = json.load(jsonData)
outFile  = open('./../formatted/deals.json', 'w')

for x in xrange(0, len(data)):
    # print "DATA: ", data[x]
    address = ""
    a = ""
    try:
        a = str(data[x]["address"])
        try:
            address = a[0:a.index("|")]
        except ValueError:
            address = a
    except KeyError:
        continue
    address = address.replace(" ", "+")
   print "MAKING LOCATION REQUEST for ", address, "..."
    
    # url = "http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false" % address
    # response = urllib2.urlopen(url)
    # jsonGeoCode = response.read()
    # print len(jsonGeoCode), " r: ", jsonGeoCode
    # reply = json.loads(jsonGeoCode)

    # lat = reply["results"][0]["geometry"]["location"]["lat"]
    # lon = reply["results"][0]["geometry"]["location"]["lng"]

    # city = reply["results"][0]["address_components"][2]["long_name"]
    # state = reply["results"][0]["address_components"][4]["long_name"]
    # zipCode = reply["results"][0]["address_components"][6]["long_name"]

    url = "http://nominatim.openstreetmap.org/search?q=%s&format=json&polygon=0&polygon_geojson=1&addressdetails=1" % address
    response = urllib2.urlopen(url)
    jsonGeoCode = response.read()
    reply = json.loads(jsonGeoCode)

    if len(reply) == 0:
        continue
    lat = reply[0].get("lat")
    if lat == None:
        continue
    lon = reply[0]["lon"]

    print "PUTTING INFO..."
    city = reply[0]["address"]["city"]
    state = reply[0]["address"]["state"]
    zipCode = reply[0]["address"]["postcode"]

    name = data[x]["description"]

    description = data[x]["details"]

    reg_price = int(float(data[x]["value"][1:]))

    max_price = int(float(data[x]["price"][1:]))

    min_price = float(max_price * random.random())

    print "PRICE MIN: ", min_price, " MAX: ", max_price

    bid_type = random.randint(0,2)

    categories = name

    days = ["0", "1"]

    fine_print = "I have no idea"

    max_per_user = random.randint(1,10)

    phonenumber = ""
    p = data[x]["phone_number"]
    try:
        phonenumber = p[0:p.index("|")]
    except ValueError:
        phonenumber = p

    print "MAKING MERCHANT REQUEST..."
    email = phonenumber + "@qaz.me"
    newMerchantData = {"name": address, "address": address.replace("+", " "), "latitude": lat, "longitude": lon, 
    "phonenumber" : phonenumber,  "email": email }
    newMerchant = urllib.urlencode(newMerchantData)
    # make POST requests
    url = "http://128.83.196.226:2048/merchants/"
    req = urllib2.Request(url, newMerchant)
    response = urllib2.urlopen(req)
    merchantCode = response.read()
    reply = json.loads(merchantCode)
    merchant_id = reply.get("id")

    print "ADDING MORE INFO..."
    num_deals = random.randint(1,10) * 10

    redemption_end_time =  "08:30:00"

    redemption_start_time = "22:30:00"

    expday = str(random.randint(10, 29))
    redemption_expiration = "2014-12-"+ expday +" 23:59:59"

    startday = str(random.randint(10, 29))
    start_date = "2013-08-"+ startday +" 08:30:00"

    endday = str(random.randint(1, 30))
    end_date = "2014-01-"+ endday + " 22:30:00"

    website = "www.google.com"

    img = "img"

    deal = json.dump({"bid_type": bid_type, "address" : address.replace("+", " "), "categories" :categories, "city" :city, "days" :days, 
        "description" :description, "fine_print": fine_print, "img" : img, "location": {"lat": lat, "lon" : lon}, 
        "max_per_user" : max_per_user, "max_price" : max_price, "merchant_id" : merchant_id, 
        "min_price" : min_price, "name" : name, "num_deals" : num_deals, "redemption_end_time" : redemption_end_time, 
        "redemption_start_time" : redemption_start_time, "redemption_expiration" : redemption_expiration, 
        "reg_price" : reg_price, "start_date" : start_date, "end_date" : end_date, "website" : website, 
        "state" : state, "zip" : zipCode }, outFile)
    print "...ADDED DEAL."
    outFile.write("\n")

jsonData.close()
outFile.close()