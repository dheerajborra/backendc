// in the midst of the switch from Balanced to Stripe, new customers need to be made
// this takes all the users in the database and creates Stripe customers for those who are missing customer ids

// hacked together with bits and pieces from azul_mysql.js

var mysql = require('mysql');
var stripe = require('stripe')("sk_live_J0pLR6pxZFvBiTdPZCgo64Oc");

var pool = mysql.createPool({
    host     : 'localhost',
    user     : 'azul',
    password : 'AzulRules',
});

function insert(table) {
    return 'INSERT INTO ' + table + ' SET ?';
}

function select_where(table, field) {
    return 'SELECT * FROM ' + table + ' WHERE ' + field + '= ?';
}

function delete_where(table, field) {
    return 'DELETE FROM ' + table + ' WHERE ' + field + '= ?';
}

function get_db() {
    return 'azul_test';
}


function execute_query(database, query, val, callback) {
    console.log('using: ' + database);
    pool.getConnection(function(err, connection) {
        var q = connection.query(('use ' + database + ';'));
        console.log(q.sql);
        q = connection.query(query, val, callback);
        console.log(q.sql);
        connection.release();
    });
}

execute_query('azul_test', "SELECT * FROM users", null, function(err, rows) {
	if (err) {
		console.log("oops select didn't work");
		console.log(err);
	} else {
		rows.forEach(function(user) {
			console.log("got user: " + JSON.stringify(user));

			if (user.stripe_customer == null) {
				stripe.customers.create({
					description: user.firstname + " " + user.lastname,
					email: user.email
				}, function(err, customer) {
					if (err) {
						console.log("couldn't create Stripe customer for this user, won't be able to make purchases!!");
					} else {
						console.log("Made stripe customer: " + JSON.stringify(customer));
						execute_query('azul_test', "UPDATE users SET stripe_customer=? WHERE id=?", [customer.id, user.id], function (err, result) {
							console.log("updated: " + JSON.stringify(result));
							if (err) {
								console.log("WARNING update didn't work for customer " + customer.id);
							}
						});
					}
				});
			} else {
				console.log("stripe customer not null, skip");
			}
		});
	}
});
