var io = require('socket.io');
var request = require('request');

var server_host = "http://localhost:2048"

var clients = {};
var userClients = {};

function performBid(data, socket) {
	return {id: data.id, price_now: data.price_now-1, price_prev: data.price_now};
}

exports.setup_socket = function(server) {
    io.listen(server, {'log level': 1}).sockets.on('connection', function (socket) {
        clients[socket.id.toString()] = socket;
        socket.user = {};

        socket.on('initialize', function (auth) {
            socket.user = auth;
			console.log('socket: initialize connection for user: ' + auth.id + ' (socket id ' + socket.id + ')');
            userClients[auth.id.toString()] = socket;
        });

        socket.on('logout', function() {
            if (socket.user.hasOwnProperty('id')) {
                delete userClients[socket.user.id.toString()];
            }
            socket.user = {};
        });

        socket.on('favorite', function (data) {
            //perform favorite
            console.log("Favorite: " + data + " for " + socket.user.id);
            request.post({
                'url': server_host + "/favorites", 
                'json': {
                    favorite: true,
                    user_id: socket.user.id,
                    deal_id: data.id
                }
            });
        });

        socket.on('trash', function (data) {
            //perform trash
            console.log("Trash: " + data + " for " + socket.user.id);
            request.post({
                'url': server_host + "/trash", 
                'json': {
                    trash: true,
                    user_id: socket.user.id,
                    deal_id: data.id
                }
            });
        });

        socket.on('suggest', function (data) {
            //perform suggest
            console.log("Add/Suggest: " + data + " for " + socket.user.id);
            request.post({
                'url': server_host + "/suggest", 
                'json': {
                    user_id: socket.user.id,
                    deal_id: data.merchant_id
                }
            });
        });

        socket.on('bid', function (data) {
            //perform bid
            var newData = performBid(data, socket);
            console.log("Bid: " + newData + " for " + socket.user.id);
            console.log('bid request' + JSON.stringify(data));
            socket.emit('updateBid', newData);
        });

        // receive a new bid from the python bid server
        // and send it on to the client
        socket.on('bid_recv', function (data) {
            console.log('bid received: ' + JSON.stringify(data));
            if (userClients[data.user_id.toString()] != undefined) {
				console.log('forwarding bid: ' + JSON.stringify(data));
                userClients[data.user_id.toString()].emit('updateBid', data);
            } else {
                console.warn("MISSING USER CLIENT -- " + data.user_id);
            }
        });

        socket.on('disconnect', function () {
            delete clients[socket.id.toString()];
            if (socket.user.hasOwnProperty('id') && userClients[socket.user.id.toString()].id == socket.id) {
				console.log('socket: user ' + socket.user.id + ' disconnected (socket id ' + socket.id + ')');
                delete userClients[socket.user.id.toString()];
            } else {
                console.log('dangling socket disconnect:' + socket.id);
            }
        });
    });
}

exports.send_bid = function(data) {
    userClients[data.user_id.toString()].emit('updateBid', data);
}
