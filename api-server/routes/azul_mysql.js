var mysql = require('mysql');
var https = require('https');
var request = require('request');
var bcrypt = require('bcrypt');
var crypto = require('crypto');
var stripe = require('stripe')("sk_live_J0pLR6pxZFvBiTdPZCgo64Oc");
var XDate = require('xdate');

var pool = mysql.createPool({
    host     : 'localhost',
    user     : 'azul',
    password : 'AzulRules',
});

// TODO: replace these with Azul app info; currently using personal test app info
var fbAppId = "523831917702051";
var fbAppSecret = "74c90fd6534b709336da6a1d207bedeb";
var fbAccessToken = "523831917702051|-rQrxJM_NTqLzD39GkcLOKE0HPs";


function insert(table) {
    return 'INSERT INTO ' + table + ' SET ?';
}

function select_where(table, field) {
    return 'SELECT * FROM ' + table + ' WHERE ' + field + '= ?';
}

function delete_where(table, field) {
    return 'DELETE FROM ' + table + ' WHERE ' + field + '= ?';
}

function get_db(req) {
    var port = req.headers.host.split(':')[1];
    if (port == 2048) {
        return 'azul_test';
    } else if (port == 2049) {
        return 'azul_mock';
    } else if (port == 2050) {
        return 'azul_prod';
    }
}


function execute_query(database, query, val, callback) {
    console.log('using: ' + database);
    pool.getConnection(function(err, connection) {
        var q = connection.query(('use ' + database + ';'));
        console.log(q.sql);
        q = connection.query(query, val, callback);
        console.log(q.sql);
        connection.release();
    });
}


exports.stat = function(req, res) {
    console.log("status requested");
    var query = 'SELECT 1+1 AS solution';
    callback = function(err, rows, fields) {
        if (err) throw err;
        console.log('MySQL is ok');
        res.send({heartbeat: 'true'});
    };
    execute_query(get_db(req), query, '', callback);
};


exports.add_user = function(req, res) {
    var user = req.body;
    // hash password
    var hash = bcrypt.hashSync(user.password, 10);
    user.password = hash;
    
    // associate user with Stripe customer
    stripe.customers.create({
        description: user.firstname + " " + user.lastname,
        email: user.email
    }, function(err, customer) {
        if (err) {
            console.warn("couldn't create Stripe customer for this user, won't be able to make purchases!!");
        } else {
            user.stripe_customer = customer.id;
        }

        console.log('adding user: ' + JSON.stringify(user));
        var query = insert('users');
        callback = function(err, result) {
            if (err) {
                console.log(err.toString());
                res.send({error: err.toString()});
            } else {
                console.log('inserted user with id: ' + result.insertId);
                res.send({id: result.insertId});
            }
        };
        execute_query(get_db(req), query, user, callback);
    });
};


exports.get_user = function(req, res) {
    var param = null,
        query = null;
    if (req.query.email) {
        param = req.query.email;
        query = select_where('users', 'email');
    } else if (req.query.facebook_id) {
        param = req.query.facebook_id;
        query = select_where('users', 'facebook_id');
    } else if (req.query.id) {
        param = req.query.id;
        query = select_where('users', 'id');
    } else if (req.params.id) {
        param = req.params.id;
        query = select_where('users', 'id');
    }

    callback = function(err, rows) {
        if (err) {
            res.send(500, {error: err.toString()});
        } else {
            if (rows.length == 0) {
                console.log("no user found");
                res.send(404, {error: "user not found"});
            } else {
                console.log('found user: ' + JSON.stringify(rows[0]));
                res.send({user: rows[0]});
            }
        }
    };

    console.log('param in get_user: ' + param);
    console.log('query in get_user: ' + query);
    execute_query(get_db(req), query, param, callback);
};


exports.delete_user = function(req, res) {
    var param = null,
        query = null;
    if (req.query.email) {
        param = req.query.email;
        query = delete_where('users', 'email');
    } else if (req.query.facebook_id) {
        param = req.query.facebook_id;
        query = delete_where('users', 'facebook_id');
    } else if (req.query.id) {
        param = req.query.id;
        query = delete_where('users', 'id');
    } else if (req.params.id) {
        param = req.params.id;
        query = delete_where('users', 'id');
    }

    callback = function(err, rows) {
        if(err) {
            console.log('error: ' + err.toString());
            res.send(500, {error: err.toString()});
        } else {
            console.log('deleted user: ' + param);
            res.send({deleted_user: param});
        }
    };

    console.log('param in delete_user: ' + param);
    console.log('query in delete_user: ' + query);
    execute_query(get_db(req), query, param, callback);
};


exports.update_user = function(req, res) {
    var id = null,
        param = [],
        query = null;
    if (req.query.id) {
        id = req.query.id;
    } else if (req.params.id) {
        id = req.params.id;
    } else if (req.body.id) {
        id = req.body.id;
    }

    var valid_fields = ["firstname", "lastname", "email", "sex", "phonenumber", "birthdate"];
    var query_fields = [];

    for (var field in valid_fields) {
        field = valid_fields[field];
        if (req.body.hasOwnProperty(field)) {
            query_fields.push(field + '=?');
            param.push(req.body[field]);
        } else if (req.params.hasOwnProperty(field)) {
            query_fields.push(field + '=?');
            param.push(req.params[field]);
        }
    }

    if (query_fields.length === 0) {
        res.send(400, {error: "no valid fields to update"});
        return;
    }

    query = "UPDATE users SET " + query_fields.toString() + " WHERE id=?";
    param.push(id);

    callback = function(err, rows) {
        if(err) {
            console.log('error: ' + err.toString());
            res.send(500, {error: err.toString()});
        } else {
            console.log('updated user: ' + id);
            res.send(200);
        }
    };

    console.log('param in delete_user: ' + param);
    console.log('query in delete_user: ' + query);
    execute_query(get_db(req), query, param, callback);
}



exports.add_user_card = function(req, res) {
    var param = req.params.id;
    query = select_where('users', 'id');
    
    execute_query(get_db(req), query, param, function(err, rows) {
        if (err) {
            console.log({error: err.toString()});
            res.send(500, {error: err.toString()});
        } else {
            if (rows.length === 0) {
                console.log({error: "user id not found"});
                res.send(404, {error: "user id not found"});
            } else {
                var user = rows[0];
                console.log('found user: ' + JSON.stringify(user));
                
                // add tokenized card to Stripe customer
                stripe.customers.createCard(user.stripe_customer, {card: req.body.token}, function(err, card) {
                    if (err) {
                        res.send(500, {error: "could not get tokenized card - " + err.message});
                    } else {
                        // store card token + some info to database
                        var userCard = {
                            'user_id' : user.id,
                            'card_token' : card.id,
                            'last_four' : card.last4,
                            'expiration_month' : card.exp_month,
                            'expiration_year' : card.exp_year,
                            'card_type' : card.type
                        };
                        query = insert('usercards');
                        
                        execute_query(get_db(req), query, userCard, function(err, result) {
                            if (err) {
                                console.log(err.toString());
                                res.send(500, {error: err.toString()});
                            } else {
                                console.log('inserted card token with id: ' + result.insertId);
                                res.send({id: result.insertId});
                            }
                        });
                    }
                });
            }
        }
    });
}


exports.get_user_cards = function(req, res) {
    var param = null,
        query = select_where('usercards', 'user_id');
    
    if (req.query.card_id) {
        param = req.query.card_id;
        query = select_where('usercards', 'card_id');
    } else if (req.params.id) {
        param = req.params.id;
        query = select_where('usercards', 'user_id');
    }

    callback = function(err, rows) {
        if (err) {
            res.send(500, {error: err.toString()});
        } else {
            console.log('found cards: ' + JSON.stringify(rows));
            res.send({cards: rows});
        }
    };

    console.log('param in get_user_cards: ' + param);
    console.log('query in get_user_cards: ' + query);
    execute_query(get_db(req), query, param, callback);
};


exports.delete_user_card = function(req, res) {
    // TODO: need to get cards and delete them from Stripe first
    var param = null,
        query = delete_where('usercards', 'card_id');

    if (req.query.card_id) {
        param = req.query.card_id;
        query = delete_where('usercards', 'card_id');
    } else if (req.params.id) {
        param = req.params.id;
        query = delete_where('usercards', 'user_id');
    }

    callback = function(err, rows) {
        if(err) {
            console.log('error: ' + err.toString());
            res.send(500, {error: err.toString()});
        } else {
            console.log('deleted cards: ' + param);
            res.send({deleted_cards: param});
        }
    };

    console.log('param in delete_user_cards: ' + param);
    console.log('query in delete_user_cards: ' + query);
    execute_query(get_db(req), query, param, callback);
};



exports.debit_user_card = function(req, res) {
    var param = req.params.id;
    var query = select_where('users', 'id');
    
    execute_query(get_db(req), query, param, function(err, rows) {
        if (err) {
            res.send(500, {error: err.toString()});
        } else {
            if (rows.length === 0) {
                res.send(404, {error: "user id not found"});
            } else {
                var user = rows[0];
                var customerId = user.stripe_customer;
                
                param = req.body.card_id;
                query = select_where('usercards', 'card_id');
                
                execute_query(get_db(req), query, param, function(err, rows) {
                    if (err) {
                        res.send(500, {error: err.toString()});
                    } else if (rows.length === 0) {
                        res.send(404, {error: "user card not found"});
                    } else {
                        var card = rows[0];
                        var cardUserId = card.user_id;
                        var cardToken = card.card_token;
                        
                        if (cardUserId != user.id) {
                            res.send(403, {error: "card user does not match specified user"});
                        } else {
                            var dealData = req.body.deal;
                            var dealMerch = dealData.merchant;
                            var dealDescription = dealData.description;
                            var dealRemaining = dealData.inventory;

                            var sDescription = "Deal brought to you by AZUL - " + dealDescription;

                            if (dealRemaining > 0) {
                                var charge = stripe.charges.create({
                                    amount: Math.floor(req.body.price * 100),
                                    currency: "usd",
                                    card: cardToken,
                                    customer: customerId,
                                    description: sDescription
                                }, function(err, charge) {
                                    if (err) {
                                        res.send(500, {error: "could not charge card - " + err.message});
                                    } else {
                                        request.post({
                                            'url': "http://" + req.headers.host + "/search", 
                                            'json': {
                                                "type": "deal",
                                                "data": {
                                                    "id": req.body.deal_id,
                                                    "decrement": true
                                                }
                                            }
                                        }, function(error, response, body) {
                                            if (error || response.statusCode != 200) {
                                                console.warn("ES post to decrement did not work - inventory might not have been decremented!!");
                                            }
                                        });

                                        var query = "SELECT * FROM vouchers WHERE deal_id=? AND voucher_used=false";
                                        var param = req.body.deal_id;

                                        execute_query(get_db(req), query, param, function(err, rows) {
                                            if (err || rows.length == 0) {
                                                res.send(500, {error: "couldn't get vouchers for deal"});
                                            } else {
                                                var voucher = rows[0];

                                                var query = "UPDATE vouchers SET voucher_used=true WHERE voucher_id=?";
                                                execute_query(get_db(req), query, voucher.voucher_id, function(err, result) {
                                                    if (err) {
                                                        console.warn("Setting voucher to used did not work -- voucher still may be set as unused!!");
                                                    }
                                                });

                                                // store purchase history
                                                var basePurchaseData = {
                                                    "user_id": user.id,
                                                    "deal_id": req.body.deal_id,
                                                    "address": dealData.address,
                                                    "category": dealData.category,
                                                    "fineprint": dealData.fineprint,
                                                    "img": dealData.img,
                                                    "voucher_img": voucher.voucher_img,
                                                    "voucher_code": voucher.voucher_code,
                                                    "merchant_id": dealData.merchant_id,
                                                    "merchant": dealMerch,
                                                    "price_now": req.body.price,
                                                    "price_reg": dealData.price_reg
                                                };

                                                var dbPurchaseData = JSON.parse(JSON.stringify(basePurchaseData));
                                                dbPurchaseData["location_lat"] = dealData.location.lat;
                                                dbPurchaseData["location_lon"] = dealData.location.lon;

                                                var query = insert('purchases');
                                                execute_query(get_db(req), query, dbPurchaseData, function(err, result) {});

                                                // send result
                                                var resultData = basePurchaseData;
                                                resultData["location"] = dealData.location;
                                                res.send(resultData);
                                            }
                                        });
                                    }
                                });
                            } else {
                                res.send(410, {error: "deal is no longer available"});
                            }
                        }
                    }
                });
            }
        }
    });
}



exports.auth_user = function(req, res) {
    var param = null, query = null;
    
    if (req.body.hasOwnProperty('email')) {    // email+password auth
        param = req.body.email;
        query = select_where('users', 'email');
        execute_query(get_db(req), query, param, function(err, rows) {
            if (err) {
                console.log('error: ' + err.toString());
                res.send(500, {error: err.toString()});
            } else {
                if (rows.length == 0) {
                    console.log("no user found");
                    res.send(404, {"error": "no user found"});
                } else {
                    var user = rows[0];
                    console.log('found user: ' + JSON.stringify(user));

                    if (bcrypt.compareSync(req.body.password, user.password)) {
                        res.send(200, {"id": user.id});
                    } else {
                        res.send(401, {"error": "password mismatch"});
                    }
                }
            }
        });
    } else if (req.body.hasOwnProperty('facebook_id')) {    // facebook id+token auth
        //verify facebook token with id
        var params = "input_token=" + req.body.facebook_token + "&access_token=" + fbAccessToken;
        
        request("https://graph.facebook.com/debug_token?" + params, function(error, response, body) {
            console.log("AUTH: fblog body = " + body);
            if (error) {
                res.send(500, {"error": error});
            } else {
                // verify details match with this app/user
                var data = JSON.parse(body).data;
                console.log("AUTH: fblog data = " + JSON.stringify(data));
                
                if (data.error) {
                    res.send(500, {"error" : data.error.message});
                } else if (!data.is_valid || data.app_id != fbAppId || data.user_id != req.body.facebook_id) {
                    res.send(401, {"error" : "Facebook token mismatch"});
                } else {
                    param = req.body.facebook_id;
                    query = select_where('users', 'facebook_id');
                    execute_query(get_db(req), query, param, function(err, rows) {
                        if (err) {
                            res.send(500, {"error": err.toString()});
                        } else {
                            if (rows.length == 0) {    // user not registered by facebook yet
                                request("https://graph.facebook.com/" + data.user_id + "?access_token=" + req.body.facebook_token, function(error, response, body) {
                                    console.log("AUTH: fbreg user = " + body);
                                    if (error) {
                                        res.send(500, {"error" : error});
                                    } else {
                                        var userData = JSON.parse(body);
                                        console.log("AUTH: fbreg data = " + JSON.stringify(userData));
                                        
                                        if (userData.error) {
                                            res.send(500, {"error" : userData.error.message});
                                        } else {
                                            var param = userData.email;
                                            var query = select_where('users', 'email');
                                            execute_query(get_db(req), query, param, function(err, rows) {
                                                if (err) {
                                                    res.send(500, {error: err.toString()});
                                                } else {
                                                    if (rows.length == 0) {    // user is completely unregistered
                                                        var bd = userData.birthday.split("/");
                                                        var regData = {
                                                            'firstname' : userData.first_name,
                                                            'lastname' : userData.last_name,
                                                            'sex' : (userData.gender == "male" ? 'M' : 'F'),
                                                            'birthdate' : bd[2] + "-" + bd[0] + "-" + bd[1],
                                                            'email' : userData.email,
                                                            // TODO: temporary fix: random password here to prevent allowing blank password login
                                                            'password' : Math.random().toString(36).slice(2),
                                                            'facebook_id' : userData.id
                                                        };
                                                        
                                                        // TODO: this needs to be changed to use Stripe in the future
                                                        // since beta isn't using Facebook login, not touching this

                                                        // associate user w/ balanced customer uri
                                                        request.post({
                                                            'headers': {
                                                                'content-type' : 'application/x-www-form-urlencoded',
                                                                'Authorization' : balancedAuth
                                                            },
                                                            'url': balancedApiUrl + "/v1/customers",
                                                            'body':
                                                                'name=' + encodeURIComponent(regData.firstname + " " + regData.lastname) +
                                                                '&email=' + encodeURIComponent(regData.email)
                                                        }, function(error, response, body) {
                                                            console.log("balanced customer post: ");
                                                            console.log(body);

                                                            if (error || response.statusCode != 201) {
                                                                res.send(response.statusCode, {error: 'could not create balanced customer'});
                                                            } else {
                                                                var customer = JSON.parse(body);
                                                                regData.balanced_uri = customer.uri;
                                                                
                                                                var query = insert('users');
                                                                execute_query(get_db(req), query, regData, function(err, result) {
                                                                    if (err) {
                                                                        console.log(err.toString());
                                                                        res.send(500, {"error": err.toString()});
                                                                    } else {
                                                                        console.log('inserted user with id: ' + result.insertId);
                                                                        res.send(200, {"id": result.insertId});
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    } else {    // user has registered with email before
                                                        // TODO: use existing user and add facebook id instead of rejecting login
                                                        res.send(500, {"error": "User already exists by email"});
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                var user = rows[0];
                                res.send(200, {"id": user.id});
                            }
                        }
                    });
                }
            }
        });
    } else {    // no user to auth?
        res.send(400, {"error" : "Missing username or facebook id"});
    }
}



exports.add_merchant = function(req, res) {
    var merchant = req.body;
    console.log('adding merchant: ' + JSON.stringify(merchant));
    var query = insert('merchants');
    callback = function(err, result) {
        if (err) {
            console.log({error: err.toString()});
            res.send(500, {error: err.toString()});
        } else {
            console.log('inserted merchant with id: ' + result.insertId);
            res.send({id: result.insertId});
        }
    };
    execute_query(get_db(req), query, merchant, callback);
};


exports.get_merchant = function(req, res) {
    var param = null,
        query = null;
    if (req.query.email) {
        param = req.query.email;
        query = select_where('merchants', 'email');
    } else if (req.query.facebook_id) {
        param = req.query.facebook_id;
        query = select_where('merchants', 'facebook_id');
    } else if (req.query.id) {
        param = req.query.id;
        query = select_where('merchants', 'id');
    } else if (req.params.id) {
        param = req.params.id;
        query = select_where('merchants', 'id');
    }

    callback = function(err, rows) {
        if (err) {
            res.send(500, {error: 'no key found'});
        } else {
            if (rows.length == 0) {
                console.log("merchant not found");
                res.send(404, {error: "merchant not found"});
            } else {
                console.log('found merchant: ' + JSON.stringify(rows[0]));
                res.send({merchant: rows[0]});
            }
        }
    };

    console.log('param in get_merchant: ' + param);
    console.log('query in get_merchant: ' + query);
    execute_query(get_db(req), query, param, callback);
};


exports.get_merchant_activedeals = function(req, res) {
    var param = req.params.id;

    // TODO: get actual merchant's active deals
    var data = {
        views: [
            {
                label: "Lunch",
                value: 40
            },
            {
                label: "Dinner",
                value: 25
            },
            {
                label: "Midnight Snack",
                value: 35
            }
        ],
        clicks: [
            {
                label: "Lunch",
                value: 20
            },
            {
                label: "Dinner",
                value: 40
            },
            {
                label: "Midnight Snack",
                value: 35
            }
        ],
        revenue: [
            {
                label: "Lunch",
                value: 12
            },
            {
                label: "Dinner",
                value: 35
            },
            {
                label: "Midnight Snack",
                value: 53
            }
        ]
    };

    res.send(data);
};


exports.get_merchant_revenue = function(req, res) {
    var param = req.params.id;
    var query = req.query;

    if (query.hasOwnProperty("start_date") && query.hasOwnProperty("end_date")) {
        // TODO: get actual merchant's revenue
        var data = {
            revenue: [
                {
                    key: "Merchant Revenue",
                    values: []
                },
                {
                    key: "Azul Revenue",
                    values: []
                }
            ],
            //average: [{
            //    key: "Average Price",
            //    values: []
            //}],
        };

        var start_time = parseInt(query.start_date);
        var end_time = parseInt(query.end_date);
        var time_scale = (query.hasOwnProperty("time_scale") ? parseInt(query.time_scale) : 86400000);

        for (var time = start_time; time <= end_time; time += time_scale) {
            // deterministic "pseudorandom" numbers
            var value1 = ("0." + Math.sin(time).toString().substr(6)) * 80 + 20;
            var value2 = ("0." + Math.cos(time).toString().substr(6)) * 10 + 5;

            data.revenue[0].values.push({x: time, y: +value1.toFixed(2)});
            data.revenue[1].values.push({x: time, y: +(value1/10).toFixed(2)});
            //data.average[0].values.push({x: time, y: +value2.toFixed(2)});
        }

        res.send(data);
    } else {
        res.send(500, {error: "missing start_date and/or end_date"});
    }
}


exports.get_merchant_demographics = function(req, res) {
    var param = req.params.id;

    // TODO: get actual merchant's demographics. these are real based on 
    // beta test data but not aggregated at the merchant level
    var data = {
        sex: [
            {
                label: "Male",
                value: 17
            },
            {
                label: "Female",
                value: 1
            },
            {
                label: "Unspecified",
                value: 22
            }
        ],
        age: [
            {
                label: "<18",
                value: 10
            },
            {
                label: "18-25",
                value: 4
            },
            {
                label: "25-35",
                value: 2
            },
            {
                label: "35-45",
                value: 1
            },
            {
                label: "45-60",
                value: 3
            },
            {
                label: ">60",
                value: 0
            },
            {
                label: "Unspecified",
                value: 23
            }
        ]
    };

    res.send(data);
};


exports.delete_merchant = function(req, res) {
    var param = null,
        query = null;
    if (req.query.email) {
        param = req.query.email;
        query = delete_where('merchants', 'email');
    } else if (req.query.facebook_id) {
        param = req.query.facebook_id;
        query = delete_where('merchants', 'facebook_id');
    } else if (req.query.id) {
        param = req.query.id;
        query = delete_where('merchants', 'id');
    } else if (req.params.id) {
        param = req.params.id;
        query = delete_where('merchants', 'id');
    }

    callback = function(err, rows) {
        if(err) {
            console.log('error: ' + err.toString());
            res.send(500, {error: err.toString()});
        } else {
            console.log('deleted merchant: ' + param);
            res.send({deleted_merchant: param});
        }
    };

    console.log('param in delete_merchant: ' + param);
    console.log('query in delete_merchant: ' + query);
    execute_query(get_db(req), query, param, callback);
};




// expect something like {favorite: {userid: <id>, dealid: <id>}}
exports.add_user_preference = function(req, res) {
    var data = null,
        table = null,
        field = null,
        del_data = null,
        other_table = null;

    console.log(req.body);

    if (req.body.favorite) {
        table = 'favorite_';
        other_table = 'trashed_';
    } else if (req.body.trash) {
        table = 'trashed_';
        other_table = 'favorite_';
    } else {
        res.send(400, {error: 'not proper json'});
    }


    if (req.body.merchant_id) {
        table += 'merchants';
        other_table += 'merchants';
        field = 'merchant_id';
        del_data = req.body.merchant_id;
        data = {user_id: req.body.user_id, merchant_id: req.body.merchant_id};
    } else if (req.body.deal_id) {
        table += 'deals';
        other_table += 'deals';
        field = 'deal_id';
        del_data = req.body.deal_id;
        data = {user_id: req.body.user_id, deal_id: req.body.deal_id};
    } else {
        res.send(400, {error: 'not proper json'});
    }

    var query = insert(table);
    callback = function(err, result) {
        if (err) {
            console.log('error: ' + err.toString());
            res.send(500, {error: err.toString()});
        } else {
            console.log('success adding:' + JSON.stringify(data));
            res.send({success: true});
        }
    };
    execute_query(get_db(req), query, data, callback);

    // if something gets marked as a favorite, we have to delete from trash
    // and vice versa
    query = 'DELETE FROM ' + other_table + ' WHERE  user_id=? AND ' + field + '=?';
    execute_query(get_db(req), query, [req.body.user_id, del_data], callback);
};



exports.get_favorite_deals = function(req, res){
    var table = 'favorite_deals';
    get_user_preferences(req, res, table);
};

exports.get_favorite_merchants = function(req, res) {
    var table = 'favorite_merchants';
    get_user_preferences(req, res, table);
};

exports.get_trashed_deals = function(req, res){
    var table = 'trashed_deals';
    get_user_preferences(req, res, table);
};

exports.get_trashed_merchants = function(req, res) {
    var table = 'trashed_merchants';
    get_user_preferences(req, res, table);
};

get_user_preferences = function(req, res, table) {
    var query = select_where(table, 'user_id'),
        data = req.params.id;

    callback = function(err, rows) {
        if (err) {
            console.log('error: ' + err.toString());
            res.send(500, {error: err.toString()});
        } else {
            console.log('success getting:' + JSON.stringify(rows));
            res.send({success: JSON.stringify(rows)});
        }
    };
    execute_query(get_db(req), query, data, callback);
};



exports.delete_user_preference = function(req, res) {
    var data = null,
        table = null,
        field = null;

    console.log(req.body);

    if (req.body.favorite) {
        table = 'favorite_';
    } else if (req.body.trash) {
        table = 'trashed_';
    } else {
        res.send(400, {error: 'not proper json'});
    }


    if (req.body.merchant_id) {
        table += 'merchants';
        data = req.body.merchant_id;
        field = 'merchant_id';
    } else if (req.body.deal_id) {
        table += 'deals';
        data = req.body.deal_id;
        field = 'deal_id';
    } else {
        res.send(400, {error: 'not proper json'});
    }

    var query = 'DELETE FROM ' + table + ' WHERE  user_id=? AND ' + field + '=?';
    console.log(query);
    callback = function(err, result) {
        if (err) {
            console.log('error: ' + err.toString());
            res.send(500, {error: err.toString()});
        } else {
            console.log('success deleting:' + JSON.stringify(data));
            res.send({success: true});
        }
    };
    execute_query(get_db(req), query, [req.body.user_id, data], callback);
};



exports.add_suggestion = function(req, res){
    var id = req.body;
    console.log('adding suggestion: ' + JSON.stringify(id));
    var query = insert('suggestion');
    callback = function(err, result) {
        if (err) {
            console.log(err.toString());
            res.send(500, {error: err.toString()});
        } else {
            res.send(200);
        }
    };
    execute_query(get_db(req), query, id, callback);
}



exports.send_feedback = function(req, res){
    var data = req.body;
    var query = insert('feedback');
    callback = function(err, result) {
        if (err) {
            console.log(err.toString());
            res.send(500, {error: err.toString()});
        } else {
            res.send(200);
        }
    };
    execute_query(get_db(req), query, data, callback);
}



exports.get_purchases = function(req, res){
    var param = req.params.id;
    var query = select_where('purchases', 'user_id');
    callback = function(err, result) {
        if (err) {
            console.log(err.toString());
            res.send(500, {error: err.toString()});
        } else {
            var list = [];
            result.forEach(function(dealData) {
                list.push({
                    "deal_id": dealData.deal_id,
                    "address": dealData.address,
                    "category": dealData.category,
                    "fineprint": dealData.fineprint,
                    "img": dealData.img,
                    "voucher_img": dealData.voucher_img,
                    "voucher_code": dealData.voucher_code,
                    "location": {
                        "lat": dealData.location_lat,
                        "lon": dealData.location_lon
                    },
                    "merchant": dealData.merchant,
                    "price_now": dealData.price_now,
                    "price_reg": dealData.price_reg
                });
            });
            res.send(list);
        }
    };
    execute_query(get_db(req), query, param, callback);
}



exports.add_bid = function(req, res){
    var bid = req.body;
    console.log('adding bid: ' + JSON.stringify(bid));
    var query = insert('bid_history');
    callback = function(err, result) {
        if (err) {
            console.log(err.toString());
            res.send(500, {error: err.toString()});
        } else {
            console.log('inserted bid with id: ' + result.insertId);
            res.send({id: result.insertId});
        }
    };
    execute_query(get_db(req), query, bid, callback);
};


exports.get_bids = function(req, res) {
    var param = null,
        query = null;

    if (req.query.user_id) {
        param = req.query.user_id;
        query = select_where('bid_history', 'user_id');
        console.log('user id');
    } else if (req.query.deal_id) {
        param = req.query.deal_id;
        query = select_where('bid_history', 'deal_id');
        console.log('deal id');
    } else if (req.query.id) {
        param = req.query.id;
        query = select_where('bid_history', 'id');
    } else if (req.params.id) {
        param = req.params.id;
        query = select_where('bid_history', 'id');
    } else {
        console.log('invalid bid format:' + req.body.toString());
        res.send(400, {error: 'invalid bid format'});
    }
    
    callback = function(err, rows) {
        if (err) {
            console.log('error: ' + err.toString());
            res.send(500, {error: err.toString()});
        } else {
            console.log('success getting bid history:' + JSON.stringify(rows));
            res.send({success: JSON.stringify(rows)});
        }
    };

    execute_query(get_db(req), query, param, callback);
};


/*
 * get average bid price for a particular deal id between start and end dates
 * optional time_scale parameter
 * deal = { age_data: gender_data: location_data: average_price_data: }
 */
exports.get_avg_deal_price = function(req, res) {

    var deal_id = null,
        query = req.query,
        start_time = null,
        end_time = null;


    if (req.params.id) {
        deal_id = req.params.id;
    } else {
        err = 'error: deal_id required to get average price';
        console.log(err);
        res.send(400, {error: err});
        return;
    }

    console.log('average deal price request for deal ' + deal_id);

    if (query.start_date && query.end_date) {
      start_time = parseInt(query.start_date);
      end_time = parseInt(query.end_date);
    } else {
      err = 'error: missing start or end date';
      console.log(err);
      res.send(400, {error: err});
      return;
    }


    // default to 1 day timestep 
    var time_scale = (query.hasOwnProperty("time_scale") ? parseInt(query.time_scale) : 86400);


    // round start time down to nearest multiple of time scale
    if (start_time % time_scale != 0) {
      start_time = start_time - (start_time % time_scale);
    }
    // round end time up to nearest multiple of time scale
    if (end_time % time_scale != 0) {
      end_time = end_time + (time_scale - end_time % time_scale);
    }

    var num_avgs = Math.ceil((end_time - start_time) / time_scale);
    console.log("length of array: " + num_avgs);
    
    var ut_campus_lat = 30.284102;
    var ut_campus_long = -97.733431;
    var num_locations = 50;
    var location_variation = 0.03;

    var result = {
        // TODO this isnt aggregated at the deal level until we pull from elasticsearch
        age_data: [
          { label: "<18", value: 10 },
          { label: "18-25", value: 4 },
          { label: "25-35", value: 2 },
          { label: "35-45", value: 1 }, 
          { label: "45-60", value: 3 },
          { label: ">60", value: 0 },
          { label: "Unspecified", value: 23 }
        ],
        // TODO this is aggregated in total over all deals, should be made deal-specific
        // pulled from elasticsearch
        gender_data: [
          { label: "Male", value: 17 },
          { label: "Female", value: 1 },
          { label: "Unspecified", value: 22 }
        ],
        location_data: randomized_location_data(ut_campus_lat, ut_campus_long, 
            location_variation, num_locations),
        average: []
    };


    // get each average price
    for (var q_start_time = start_time; q_start_time < end_time; q_start_time += time_scale) {
      var q_end_time = q_start_time + time_scale;
      console.log(query);
      get_avg_deal_price_for_time(deal_id, q_start_time, q_end_time, req, res, result, num_avgs);
    }


};


function randomized_location_data(lat_center, long_center, diff, num_results) {
  var result = [];
  for (var i = 0; i < num_results; i++) {
    var lat = lat_center + Math.random() * 2 * diff - diff;
    var lng = long_center + Math.random() * 2 * diff - diff;
    result.push({latitude: lat, longitude: lng});
  }

  return result;
}

function get_avg_deal_price_for_time(deal_id, start_time, end_time, req, res, result, exp_length) {
    
    callback = function(err, rows) {
        if (err) {
            console.log('error: ' + err.toString());
            res.send(500, {error: err.toString()});
        } else {
            console.log('success getting average bid:' + JSON.stringify(rows[0]) + ' ' + start_time);
            avg_price = rows[0]["AVG(new_price)"];

            // no bids for the deal
            if (avg_price == null) {
                avg_price = 0.0;
            }

            result.average.push({x: start_time, y: avg_price});

            // if all the average prices for the week have been added, send the response
            if (Object.keys(result.average).length == exp_length) {
                res.send(result);
            }
        }
    };

    var query =  "SELECT AVG(new_price) FROM bid_history WHERE `deal_id`='" + deal_id + 
        "' AND `time` BETWEEN FROM_UNIXTIME('" + start_time +
        "') AND FROM_UNIXTIME('" + end_time + "');";
    execute_query(get_db(req), query, null, callback);
}


exports.size_of_table= function(req, res) {
  console.log("hi from get size of table");
  /*
  connection.connect();
  
  console.log(req.params.table);
  var sql = "SELECT COUNT(1) FROM ??";
  var inserts = ['users'];
  connection.query('use azul_test');
  connection.query('SELECT COUNT(1) FROM users as table_size', function(err, rows, fields) {
    if (err) throw err;
      console.log('table size for users: ', rows[0].table_size);
      res.send({table_name: "users", description: rows[0].table_size});
  });
  connection.end()
  */
};
