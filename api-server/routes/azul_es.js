/*
 * interface with elastic search
 * get deal query results, kick off bidding algorithm
 * 
 * NOTE: before running, execute on backendc/:
 * this will build a .jar containing our elasticsearch client
 * mvn clean compile assembly:single
 *
 * @author: sudheesh katkam, andrew lin, tomas mccandless
 * sudheesh@azul.me, andrew@azul.me, tdm@azul.me
 */

var azul_bidder = require('./azul_bidder.js');
var spawn = require('child_process').spawn;



exports.get = function(req, res) {
    console.log('stringify(req.body): ' + JSON.stringify(req.body));
	console.log('searching for: ' + req.body.data.search_query);
	var query_process = spawn('java', ['-cp', '/home/sriram/backendc/target/backendc-1.0-SNAPSHOT-jar-with-dependencies.jar', 
			'com.azul.elastic.ElasticClient', '--get', JSON.stringify(req.body)]);

	var query_results = '';

	query_process.stdout.on('data', function (data) {
			console.log('node: got response from java ElasticClient');
			query_results = query_results + data;
			});

	query_process.stderr.on('data', function(data) {
			console.log(data.toString());
			res.send(500, {error:data.toString()});

			});

	query_process.on('close', function (code) {
			console.log('node: java child process exited with code ' + code);

			try{
			var es_obj = JSON.parse(query_results);
			} catch (e) {
			console.log(e.toString());
			res.send({error: 'tried to parse: ' + query_results + ' result: ' + e.toString()})
			return;
			}

			// kick off the bidding process if creating new search
			if (req.body.type == "deal") {
			var query_id = es_obj.query_id;
			console.log('node: bid request for query: ' + query_id);
			azul_bidder.bid(query_id, es_obj);
			}

			res.send(es_obj);
			});
};


// To put_json_object (deal, query) in the format as mentioned in end points doc
exports.put = function(req, res) {
	console.log('put search query:: ' + req.body.data);
	var query_process = spawn('java', ['-cp', '/home/sriram/backendc/target/backendc-1.0-SNAPSHOT-jar-with-dependencies.jar', 
			'com.azul.elastic.ElasticClient', '--put', JSON.stringify(req.body)]);

	query_process.stdout.on('data', function (data) {
			console.log('node: got response from java ElasticClient: ' + data.toString());
			});

	query_process.stderr.on('data', function(data) {
			console.log(data.toString());
			res.send({error:data.toString()});
			});

	query_process.on('close', function (code) {
			console.log('node: java child process exited with code ' + code);
			if (code == 0) {
			res.send(200);
			} else {
			res.send({error: 'return code from java process: ' + code});
			}
			});
};
