/*
 * make requests to the python flask server
 * to kick off the bidding process.
 *
 * @author tomas mccandless
 * tdm@azul.me
 *
 * TODO get output from the bidding algorithm and send to front end
 * via sockets
 */

var request = require('request');

bid_server_callback = function(response) {

    /*
    if (err) {
        console.error('flask error: ' + err.toString());
        console.error('response from flask server: ' + response);
    } else {
        console.log(response);
        response.send(200, 'started bidding for query_id: ' + query_id)
    }
    */

    var str = '';
    response.on('data', function (chunk) {
        str += chunk;
    });

    response.on('end', function () {
        console.log(str);
    });
}



exports.bid = function(query_id, es_obj) {

    //var bid_server = "http://128.83.196.226:5000/bid/";
    var bid_server = "http://localhost:5000/bid/";

    request.post({
        "headers": {
            "content-type" : "application/json"
        },
        "url": bid_server + query_id,
        "json" : es_obj
    }, function(error, response, body) {
        if (error) {
            var s = {error: error.toString()};
            console.log(s);
        } else {
            var s = 'node: sent query_id ' + query_id + ' to bid server';
            console.log(s);
        }
    });

};
