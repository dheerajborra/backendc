var cluster = require('cluster');
var express = require('express'),
    path = require('path'),
    http = require('http'),
    azul_mysql = require('./routes/azul_mysql'),
    azul_es = require('./routes/azul_es'),
    azul_bidder = require('./routes/azul_bidder.js'),
    socket_server = require('./socket-server.js');
 

// only the master instance should start http servers
if (cluster.isMaster) {
  setup_servers();
}


/*
 * set up the endpoints for api and start servers listening on separate ports 
 * for each database.
 * only the master instance of node should call this.
 */
function setup_servers() {
  var app = express();
   
  app.configure(function () {
      app.set('port', process.env.PORT || 2048);
      app.use(express.logger('dev')); /* 'default', 'short', 'tiny', 'dev' */
      app.use(express.bodyParser());
      app.use(express.static(path.join(__dirname, 'public')));
  });

  // allow CORS for development
  var allowCrossDomain = function(req, res, next) {
      res.header('Access-Control-Allow-Origin', '*');
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      //
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
      res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
      // intercept OPTIONS method
      if ('OPTIONS' == req.method) {
          res.send(200);
      } else {
          next();
      }
  };
  app.use(allowCrossDomain);


  /*
   * functions interfacing with mysql
   */
  app.get('/stat', azul_mysql.stat);
  //app.get('/size_of_table/:table', azul_mysql.size_of_table);


  /* 
   * post and search for users
   */
  app.post('/users', azul_mysql.add_user);
  app.get('/users', azul_mysql.get_user);
  app.get('/users/:id', azul_mysql.get_user);
  app.delete('/users', azul_mysql.delete_user);
  app.delete('/users/:id', azul_mysql.delete_user);
  app.put('/users', azul_mysql.update_user);
  app.put('/users/:id', azul_mysql.update_user);
  // TODO update user, num_users
  app.post('/authenticate', azul_mysql.auth_user);

  // payment
  app.post('/users/:id/cards', azul_mysql.add_user_card);
  app.get('/users/:id/cards', azul_mysql.get_user_cards);
  app.delete('/users/:id/cards', azul_mysql.delete_user_card);
  app.post('/users/:id/debit', azul_mysql.debit_user_card);


  /*
   * post and search for merchants
   */
  app.post('/merchants', azul_mysql.add_merchant);
  app.get('/merchants', azul_mysql.get_merchant);
  app.get('/merchants/:id', azul_mysql.get_merchant);
  app.get('/merchants/:id/activedeals', azul_mysql.get_merchant_activedeals);
  app.get('/merchants/:id/revenue', azul_mysql.get_merchant_revenue);
  app.get('/merchants/:id/demographics', azul_mysql.get_merchant_demographics);
  app.delete('/merchants/', azul_mysql.delete_merchant);
  app.delete('/merchants/:id', azul_mysql.delete_merchant);
  // TODO update merchant, num_merchants


  /*
   * user favorites and trash
   */
  // {favorite: True, user_id: <id>, deal_id: <id>}
  // {favorite: True, user_id: <id>, merchant_id: <id>}
  app.post('/favorites', azul_mysql.add_user_preference);
  app.get('/favorite_deals/:id', azul_mysql.get_favorite_deals);
  app.get('/favorite_merchants/:id', azul_mysql.get_favorite_merchants);
  app.delete('/favorites', azul_mysql.delete_user_preference);
  // {trash: True, user_id: <id>, deal_id: <id>}
  // {trash: True, user_id: <id>, merchant_id: <id>}
  app.post('/trash', azul_mysql.add_user_preference);
  app.get('/trashed_deals/:id', azul_mysql.get_trashed_deals);
  app.get('/trashed_merchants/:id', azul_mysql.get_trashed_merchants);
  app.delete('/trash', azul_mysql.delete_user_preference);

  app.post('/suggest', azul_mysql.add_suggestion);

  app.post('/feedback', azul_mysql.send_feedback);

  app.get('/purchases/:id', azul_mysql.get_purchases);

  /*
   * bid history
   */
  app.post('/bid_history', azul_mysql.add_bid);
  app.get('/bid_history', azul_mysql.get_bids);
  app.get('/bid_history/:id', azul_mysql.get_bids);
  // given a deal id, return an array of length 7, 
  // containing average prices over the past week (including today)
  app.get('/avg_deal_price/:id/', azul_mysql.get_avg_deal_price);


  /*
   * functions dealing with elasticsearch
   */
  //app.get('/search', azul_es.get);
  app.post('/search', azul_es.get);
  app.post('/deals', azul_es.put);


  // create separate servers for testing, simulation, and production
  http.createServer(app).listen(2049, function () {
     console.log('azul_mock server listening on port 2049');
  });
  http.createServer(app).listen(2048, function () {
     console.log('azul_test server listening on port 2048');
  });
  http.createServer(app).listen(2050, function () {
     console.log('azul_prod server listening on port 2050');
  });
  
  // create socket server
  socket_server.setup_socket(http.createServer(app).listen(8181, function() {
      console.log('socket server listening on port 8181');
  }));
}
