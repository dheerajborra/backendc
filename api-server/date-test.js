var http = require('http');

callback = function(response) {
    var str = '';
    response.on('data', function (chunk) {
        str += chunk;
    });

    response.on('end', function () {
        console.log(str);
    });
}

var deal_id = '93G-JF63Te2UxaefvC7-FQ';

test_avg_price = function(deal_id) {
    var server_options = {
        // prod server
        //host:'128.83.196.226',
        // test server
        //host: '128.83.52.246',
        host:'localhost',
        path:'/avg_deal_price/' + deal_id + '/?start_date=1388188800&end_date=1388793600',
        //path: '/merchants/21/revenue?start_date=1386460800000&end_date=1386547200000',
        port:'2048',
        method:'GET'
    };
    console.log(server_options.path);
    var req = http.request(server_options, callback);
    req.write('');
    req.end();
};

test_avg_price(deal_id);
