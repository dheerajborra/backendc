/*
/*
 * make requests to the python flask server
 * to kick off the bidding process.
 *
 * @author tomas mccandless
 * tdm@azul.me
 *
 * TODO get output from the bidding algorithm and send to front end
 * via sockets
 */

var http = require('http');

bid_server_callback = function(response) {

    /*
    if (err) {
        console.error('flask error: ' + err.toString());
        console.error('response from flask server: ' + response);
    } else {
        console.log(response);
        response.send(200, 'started bidding for query_id: ' + query_id)
    }
    */

    var str = '';
    response.on('data', function (chunk) {
        str += chunk;
    });

    response.on('end', function () {
        console.log(str);
    });
}



exports.bid = function(query_id) {

    var bid_server_options = {
        //host:'128.83.196.226',
        host:'localhost',
        path:'/bid/' + query_id,
        port:'5000',
        method:'POST'
    };

    var req = http.request(bid_server_options, bid_server_callback);
    req.write('');
    req.end();
}
/*
 * test the connection to the flask server
 */

var http = require('http');

//var query_id = '5-9NL0j8QOeQGj4nKIaWCg'
var query_id = 'hlpTYkVaQraWlhyVHXKBPg'
exports.bid(query_id);
