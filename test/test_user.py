import unittest
from mock import *

class TestUsers(unittest.TestCase):

    def test_str(self):
        u = mock_user1()
        assert(str(u) == "uid: 0 firstname: a lastname: b sex: M karma: 0 email: a@a.com facebook_id: fb001")
        

    def test_json(self):
        u = mock_user2()
        u_json = u.encode_json()
        u_prime = User.decode_json(u_json)
        assert str(u) == str(u_prime)

if __name__ == "__main__":
    unittest.main() # run all tests
