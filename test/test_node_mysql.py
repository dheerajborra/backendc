import unittest
import requests
import sys
sys.path.append("./src/")
from core.deal import *
from core.user import *
from core.merchant import *
from core.user_mark import *
from core.bid import *
from mock import *
from time import time


"""
unit tests for our mysql api

@author tomas mccandless
tdm@azul.me
"""


node_server = 'http://localhost:2048'

class TestNodeMySQL(unittest.TestCase):

    # make sure mysql and node are running
    def test1(self):
        r = requests.get(node_server + '/stat')
        assert r.status_code == 200
      

class TestUserAPI(TestNodeMySQL):
    userids = []

    # put 3 users. user table should be empty at this time
    def setUp(self):
        users = [mock_user1(), mock_user2(), mock_user3()]
        self.userids = []
        for user in users:
            r = requests.post(node_server + '/users', data = user.as_json_for_insertion())
            # make sure the user actually got inserted, api will return the uid 
            assert u'id' in r.json()
            self.userids.append(r.json()[u'id'])
            assert r.status_code == 200

    # should delete the 3 users. some may already be deleted, as when testing the delete functions
    def tearDown(self):
        for userid in self.userids:
            r = requests.delete(node_server + '/users/' + str(userid))
            assert r.status_code == 200


    # put a user, then assert that the table has grown in size
    def test_putuser(self):
        u = User(0, "john", "smith", Sex.MALE, 0, "john@smith.com", "passwd", "512-123-4557", "1906-11-29", "-", "fb008", "")
        r = requests.post(node_server + '/users', data=u.as_json_for_insertion())
        assert r.status_code == 200
        assert u'id' in r.json()
        # add the id so teardown will remove this user too
        self.userids.append(r.json()[u'id'])
        # try to add again, make sure theres an error (emails need to be unique)
        r = requests.post(node_server + '/users', data=u.as_json_for_insertion())
        assert 'error' in r.json()

    
    # try to lookup user in each different way
    def test_userlookup(self): 
        r1 = requests.get(node_server + '/users/' + str(self.userids[0]))
        assert r1.status_code == 200
        payload = {'id' : self.userids[0]}
        r2 = requests.get(node_server + '/users', params=payload)
        payload = {'email': 'a@a.com'}
        r3 = requests.get(node_server + '/users', params=payload)
        payload = {'facebook_id': 'fb001'}
        r4 = requests.get(node_server + '/users', params=payload)
        assert r1.text == r2.text
        assert r2.text == r3.text
        assert r3.text == r4.text

    
    def test_user_delete(self):
        payload = {'id' : self.userids[0]}
        r = requests.delete(node_server + '/users', params=payload)



# test putting, looking up merchants
class TestMerchantAPI(TestNodeMySQL):
    merchantids = []

    def setUp(self):
        merchants = [mock_merchant1(), mock_merchant2(), mock_merchant3()]
        self.merchantids = []
        for merchant in merchants:
            r = requests.post(node_server + '/merchants', data = merchant.as_json_for_insertion())
            # make sure the merchant actually got inserted, api will return the uid 
            assert u'id' in r.json()
            self.merchantids.append(r.json()[u'id'])
            assert r.status_code == 200


    def tearDown(self):
        for merchantid in self.merchantids:
            r = requests.delete(node_server + '/merchants/' + str(merchantid))
            assert r.status_code == 200


    def test_putmerchant(self):
        m = Merchant(0, "Ice Land", "84 North Lane", -1, -1, ["ice cream"], \
                    "512-432-0987", "ice@landofice.com", 4, 1.0, '-')
        r = requests.post(node_server + '/merchants', data=m.as_json_for_insertion())
        assert r.status_code == 200
        
        assert u'id' in r.json()
        self.merchantids.append(r.json()[u'id'])
        # try to add again, make sure theres an error
        r = requests.post(node_server + '/merchants', data=m.as_json_for_insertion())
        assert 'error' in r.json()


    # try to lookup merchant
    def test_merchantlookup(self): 
        r1 = requests.get(node_server + '/merchants/' + str(self.merchantids[0]))
        assert r1.status_code == 200
        payload = {'email' : 'torchys@torchys.com'}
        r2 = requests.get(node_server + '/merchants', params=payload)
        assert r2.status_code == 200
        payload = {'id' : self.merchantids[0]}
        r3 = requests.get(node_server + '/merchants', params=payload)
        assert r3.status_code == 200
        assert r1.text == r2.text
        assert r2.text == r3.text


# test marking deals/merchants as favorite/trash
class TestFaveAPI(TestNodeMySQL):

    def setUp(self):
        um = UserMark.favorite_merchant(1, 2)
        r = requests.post(node_server + '/favorites', data=um.as_json_for_insertion())
        assert 'success' in r.json()
        um = UserMark.favorite_deal(1,4)
        r = requests.post(node_server + '/favorites', data=um.as_json_for_insertion())
        assert 'success' in r.json()
        um = UserMark.trash_merchant(1,3)
        r = requests.post(node_server + '/trash', data=um.as_json_for_insertion())
        assert 'success' in r.json()
        um = UserMark.trash_deal(1,5)
        r = requests.post(node_server + '/trash', data=um.as_json_for_insertion())
        assert 'success' in r.json()

    def tearDown(self):
        um = UserMark.favorite_merchant(1, 2)
        r = requests.delete(node_server + '/favorites', data=um.as_json_for_insertion())
        assert 'success' in r.json()
        um = UserMark.favorite_deal(1,4)
        r = requests.delete(node_server + '/favorites', data=um.as_json_for_insertion())
        assert 'success' in r.json()
        um = UserMark.trash_merchant(1,3)
        r = requests.delete(node_server + '/trash', data=um.as_json_for_insertion())
        assert 'success' in r.json()
        um = UserMark.trash_deal(1,5)
        r = requests.delete(node_server + '/trash', data=um.as_json_for_insertion())
        assert 'success' in r.json()

    def test_lookup(self):
        r = requests.get(node_server + '/favorite_deals/1')
        assert 'success' in r.json()



class TestBidHistoryAPI(TestNodeMySQL):

    def setUp(self):
        t = time()
        b = Bid(1, False, 2, 3, 4.50, 4.00, 5, t)
        r = requests.post(node_server + '/bid_history', data = b.as_json_for_insertion())
        assert 'id' in r.json()


    def test_lookup(self):
        r = requests.get(node_server + '/bid_history/1')
        assert 'success' in r.json()
        payload = {'id' : 1}
        r = requests.get(node_server + '/bid_history', params=payload)
        assert 'success' in r.json()
        payload = {'user_id' : 2}
        r = requests.get(node_server + '/bid_history', params=payload)
        assert 'success' in r.json()
        payload = {'deal_id' : 3}
        r = requests.get(node_server + '/bid_history', params=payload)
        assert 'success' in r.json()

if __name__ == "__main__":
    unittest.main() # run all tests
