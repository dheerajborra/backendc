import sys
sys.path.append("./src/")
from core.merchant import *
from core.user import *

def mock_merchant1():
    return Merchant(0, "Torchy's", "100 Ash St", -1, -1, ["tacos", "mexican"], \
                    "512-123-4567", "torchys@torchys.com", 4, 1.5, '-')

def mock_merchant2():
    return Merchant(0, "Amy's Wine Bar", "123 Fake St", -1, -1, ["wine", "bars"], \
                    "314-159-2563", "admin@amyswine.com", 3, 2.0, '-')

def mock_merchant3():
    return Merchant(0, "Bacon Palace", "47 Bliss Ave", -1, -1, ["American"],\
                    "512-432-9830", "bacon@bacon.com", 3, 1, 1.0)


def mock_user1():
    return User(0, "a", "b", Sex.MALE, 0, "a@a.com", "", "512-123-4557", "1906-11-29", \
                "", "fb001", "")

def mock_user2():
    return User(0, "jane", "smith", Sex.FEMALE, 0, "jane@a.com", "", "512-123-4557", "1906-11-29", \
                "", "fb002", "")

def mock_user3():
    return User(0, "elizabeth", "smith", Sex.FEMALE, 0, "elizabeth@gmail.com", "", "512-123-5457", "1906-11-29", \
                "", "fb003", "")
