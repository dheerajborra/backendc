import unittest
import sys
sys.path.append("./src/")
from core.user_mark import *

class TestUserMarks(unittest.TestCase):


    # try the manual constructor
    def test1(self):
        um = UserMark(1, 2, True, True)
        assert(str(um) == "user 1 favorited deal 2")


    def test2(self):
        um = UserMark(1, 2, False, True)
        assert(str(um) == "user 1 trashed deal 2")


    def test3(self):
        um = UserMark(1, 2, True, False)
        assert(str(um) == "user 1 favorited merchant 2")


    def test4(self):
        um = UserMark(1, 2, False, False)
        assert(str(um) == "user 1 trashed merchant 2")


    # try the alternate class method constructors
    def test5(self):
        um = UserMark.favorite_deal(1, 2)
        assert(str(um) == "user 1 favorited deal 2")


    def test6(self):
        um = UserMark.trash_deal(1, 2)
        assert(str(um) == "user 1 trashed deal 2")


    def test7(self):
        um = UserMark.favorite_merchant(1, 2)
        assert(str(um) == "user 1 favorited merchant 2")


    def test8(self):
        um = UserMark.trash_merchant(1, 2)
        assert(str(um) == "user 1 trashed merchant 2")

    def test_json(self):
        um = UserMark.favorite_merchant(1, 2)
        dct = um.as_json_for_insertion()
        assert 'favorite' in dct
        assert 'merchant_id' in dct
        assert 'user_id' in dct
        assert len(dct.keys()) == 3




if __name__ == "__main__":
    unittest.main() # run all tests
