import unittest
from mock import *

class TestMerchant(unittest.TestCase):

    def test_str(self):
        m = mock_merchant1();
        assert(str(m) == "id: 0 name: Torchy's address: 100 Ash St email: torchys@torchys.com price: 1.5")

    def test_json_for_insertion(self):
        m = mock_merchant1()
        m_json = m.encode_json()
        m_prime = Merchant.decode_json(m_json)
        assert(str(m) == str(m_prime))

if __name__ == "__main__":
    unittest.main() # run all tests
