#!/usr/bin/python

import datetime
import json
import os
import random
import requests
import sys
import time


hosts = { 'local' : 'localhost',
          'prod'  : '128.83.196.226',
          'dev'   : '128.83.52.246' }

port = '2049'


# TODO randomize user id, query search string, and date 
def build_query():
    es_query = { "type":"deal",
        "data": { "date_time": "2013-12-25T13:40:00",
            "distance": 20, 
            "location": { "lat": 30.25, "lon": -97.7809035011583 },
            "price": 20,
            "search_query": "mexican",
            "user_id": 22
        }}

    return es_query


def main():
    print sys.argv
    usage = 'usage: execute_test_query.py <local|prod|dev> <num_queries>'
    if len(sys.argv) < 2:
        print usage
        sys.exit(1)

    host_key = sys.argv[1]
    
    if host_key not in hosts:
        print usage
        sys.exit(1)

    host = hosts[host_key]
    headers = {'content-type': 'application/json'}
    url = 'http://%s:%s/search/' % (host, port)

    num_queries = int(sys.argv[2])

    for i in xrange(num_queries):
        print 'starting query %d of %d' % (i+1, num_queries)
        es_query = build_query()
        r = requests.post(url, data=json.dumps(es_query), headers=headers)
        time.sleep(1)


main()
