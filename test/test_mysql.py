import unittest
import sys
sys.path.append("./src/")
from db_api.mysql_api import *
from core.deal import *
from core.user import *
from core.merchant import *
from core.user_mark import *
from core.bid import *

class TestMySQL(unittest.TestCase):

    # basically just test getting a connection
    def test1(self):
        num_users = size_of_table("users")
        assert True
      

    # put a user, then assert that the table has grown in size
    def test2(self):
        truncate_table("users")
        num_users = size_of_table("users")
        u = User(0, "john", "smith", Sex.MALE, 0, "a@a.com", "passwd", "512-123-4557", "1906-11-29", "-", "fb001", "")
        put_user(u)
        new_num_users = size_of_table("users")
        assert new_num_users > num_users


    # when we put a user, return the user id created
    def test_put_user(self):
        truncate_table("users")
        u = User(0, "john", "smith", Sex.MALE, 0, "a@a.com", "passwd", "512-123-4557", "1906-11-29", "-", "fb001", "")
        userid = put_user(u)
        assert(userid == 1)
        

    # put a merchant, assert the table has grown in size
    def test3(self):
        truncate_table("merchants")
        num_merchants = size_of_table("merchants")
        m = Merchant(0, "Torchy's", "100 Ash St", ["tacos", "mexican"], \
                "512-123-4567", "torchys@torchys.com", 4, 1.5, '-')
        put_merchant(m)
        new_num_merchants = size_of_table("merchants")
        assert new_num_merchants > num_merchants
    
    
    # when we put a merchant, return the merchant id created
    def test_put_merchant(self):
        truncate_table("merchants")
        m = Merchant(0, "Torchy's", "100 Ash St", ["tacos", "mexican"], \
                "512-123-4567", "torchys@torchys.com", 4, 1.5, '-')
        merchantid = put_merchant(m)
        assert(merchantid == 1)
        

    # test getting user by id
    def test4(self):
        u = User(1, "john", "smith", Sex.MALE, 0, "a@a.com", "passwd", "512-123-4557", "1906-11-29", "-", "fb001", "")
        uprime = get_user_from_id(1)
        assert(str(u) == str(uprime))


    # test getting user by email
    def test5(self):
        u = User(1, "john", "smith", Sex.MALE, 0, "a@a.com", "passwd", "512-123-4557", "1906-11-29", "-", "fb001", "")
        uprime = get_user_from_email("a@a.com")
        assert(str(u) == str(uprime))


    # test getting user by fb id
    def get_get_used_by_facebook_id(self):
        u = User(1, "john", "smith", Sex.MALE, 0, "a@a.com", "passwd", "512-123-4557", "1906-11-29", "-", "fb001", "")
        uprime = get_user_from_facebook_id("fb001")
        assert(str(u) == str(uprime))


    # test getting merchant by id
    def test6(self):
        m = Merchant(1, "Torchy's", "100 Ash St", ["tacos", "mexican"], \
                "512-123-4567", "torchys@torchys.com", 4, 1.5, '-')
        mprime = get_merchant_from_id(1)
        assert(str(m) == str(mprime))


    # test getting merchant by email
    def test7(self):
        m = Merchant(1, "Torchy's", "100 Ash St", ["tacos", "mexican"], \
                "512-123-4567", "torchys@torchys.com", 4, 1.5, '-')
        mprime = get_merchant_from_email("torchys@torchys.com")
        assert(str(m) == str(mprime))


    # clear user table, then assert its size is 0
    def test8(self):
        truncate_table("users", DB.TEST)
        assert size_of_table("users") == 0


    # clear merchant table, then assert its size is 0
    def test9(self):
        truncate_table("merchants", DB.TEST)
        assert size_of_table("merchants") == 0



    """
    test marking stuff as favorite/trash
    """
    # mark a deal as a favorite, assert it got stored
    def test10(self):
        old_num_faves = size_of_table("favorite_deals")
        um = UserMark.favorite_deal(1,2)
        add_preference_for_user(um)
        new_num_faves = size_of_table("favorite_deals")
        assert new_num_faves > old_num_faves


    # mark a merchant as favorite, assert it got stored
    def test11(self):
        old_num_faves = size_of_table("favorite_merchants")
        um = UserMark.favorite_merchant(1,3)
        add_preference_for_user(um)
        new_num_faves = size_of_table("favorite_merchants")
        assert new_num_faves > old_num_faves


    # mark a deal as trash, assert it got stored
    def test12(self):
        old_num_trash = size_of_table("trashed_deals")
        um = UserMark.trash_deal(1,4)
        add_preference_for_user(um)
        new_num_trash = size_of_table("trashed_deals")
        assert new_num_trash > old_num_trash
        

    # mark a merchant as trash, assert it got stored
    def test13(self):
        old_num_trash = size_of_table("trashed_merchants")
        um = UserMark.trash_merchant(1,4)
        add_preference_for_user(um)
        new_num_trash = size_of_table("trashed_merchants")
        assert new_num_trash > old_num_trash
        
        
    def test_delete(self):
        truncate_table("favorite_deals")
        um = UserMark.favorite_deal(1, 3)
        add_preference_for_user(um)
        delete_preference_for_user(um)
        assert size_of_table("favorite_deals") == 0



    """
    test getting favorite/trash deals/merchants
    """
    # test getting favorite deals
    def test14(self):
        truncate_table("favorite_deals")
        um = UserMark.favorite_deal(1,3)
        add_preference_for_user(um)
        um = UserMark.favorite_deal(1,4)
        # add twice to test distinctness
        add_preference_for_user(um)
        add_preference_for_user(um)
        faves = get_favorite_deals_from_userid(1)
        assert faves == [3,4]


    # test getting favorite merchants
    def test15(self):
        truncate_table("favorite_merchants")
        um = UserMark.favorite_merchant(1,3)
        add_preference_for_user(um)
        um = UserMark.favorite_merchant(1,4)
        # add twice to test distinctness
        add_preference_for_user(um)
        add_preference_for_user(um)
        faves = get_favorite_merchants_from_userid(1)
        assert faves == [3,4]
        

    # test getting trash deals
    def test16(self):
        truncate_table("trashed_deals")
        um = UserMark.trash_deal(1,3)
        add_preference_for_user(um)
        um = UserMark.trash_deal(1,4)
        # add twice to test distinctness
        add_preference_for_user(um)
        add_preference_for_user(um)
        trash = get_trash_deals_from_userid(1)
        assert trash == [3,4]


    # test getting trashed merchants
    def test17(self):
        truncate_table("trashed_merchants")
        um = UserMark.trash_merchant(1,3)
        add_preference_for_user(um)
        um = UserMark.trash_merchant(1,4)
        # add twice to test distinctness
        add_preference_for_user(um)
        add_preference_for_user(um)
        trash = get_trash_merchants_from_userid(1)
        assert trash == [3,4]



    """
    users cant both favorite and trash the same item
    """
    # trash, then favorite a deal. assert the favorite exists but trash doesnt
    def test18(self):
        truncate_table("favorite_deals")
        truncate_table("trashed_deals")
        um = UserMark.trash_deal(1,3)
        add_preference_for_user(um)
        um = UserMark.favorite_deal(1,3)
        add_preference_for_user(um)
        faves = get_favorite_deals_from_userid(1)
        trash = get_trash_deals_from_userid(1)
        assert 3 in faves
        assert 3 not in trash


    # favorite, then trash a deal. assert the trash exists but favorite doesnt
    def test19(self):
        truncate_table("favorite_deals")
        truncate_table("trashed_deals")
        um = UserMark.favorite_deal(1,3)
        add_preference_for_user(um)
        um = UserMark.trash_deal(1,3)
        add_preference_for_user(um)
        faves = get_favorite_deals_from_userid(1)
        trash = get_trash_deals_from_userid(1)
        assert 3 not in faves
        assert 3 in trash


    # trash, then favorite a merchant. assert the fave exists but the trash doesnt
    def test20(self):
        truncate_table("favorite_merchants")
        truncate_table("trashed_merchants")
        # trash, then favorite. assert the favorite exists but trash doesnt
        um = UserMark.trash_merchant(1,3)
        add_preference_for_user(um)
        um = UserMark.favorite_merchant(1,3)
        add_preference_for_user(um)
        faves = get_favorite_merchants_from_userid(1)
        trash = get_trash_merchants_from_userid(1)
        assert 3 in faves
        assert 3 not in trash


    # favorite, then trash a merchant. assert the trash exists but the fave doesnt
    def test21(self):
        truncate_table("favorite_merchants")
        truncate_table("trashed_merchants")
        # trash, then favorite. assert the favorite exists but trash doesnt
        um = UserMark.favorite_merchant(1,3)
        add_preference_for_user(um)
        um = UserMark.trash_merchant(1,3)
        add_preference_for_user(um)
        faves = get_favorite_merchants_from_userid(1)
        trash = get_trash_merchants_from_userid(1)
        assert 3 not in faves
        assert 3 in trash


    # truncate all the favorite/trash tables
    def test_truncate(self):
        tables =  ["favorite_deals", "favorite_merchants", \
                   "trashed_deals", "trashed_merchants"]
        for table in tables:
            truncate_table(table)
            assert size_of_table(table) == 0


    """
    test stuff with bid history
    """
    def test_insert_bid(self):
        old_num_bids = size_of_table("bid_history")
        b = Bid(-1, 1, 2, 4.50, 4.00)
        put_bid(b)
        new_num_bids = size_of_table("bid_history")
        assert new_num_bids > old_num_bids

    def setup_test_data(self):
        truncate_table("bid_history")
        b = Bid(-1, 1, 2, 4.50, 4.00)
        put_bid(b)
        b = Bid(-1, 2, 3, 4.50, 4.00)
        put_bid(b)
        b = Bid(-1, 2, 3, 4.00, 3.99)
        put_bid(b)
        assert size_of_table("bid_history") > 0
        

    def test_get_bids_for_deal(self):
        self.setup_test_data()

        # try getting with a deal id
        bids = get_bids_for_deal(3)
        b = bids[0]
        assert(str(b) == "user: 2 deal: 3 old_price: 4.50 new_price: 4.00")
        b = bids[1]
        assert(str(b) == "user: 2 deal: 3 old_price: 4.00 new_price: 3.99")
        
        """
        TODO fix this call to the deal constructor
        # now try getting with a deal obj
        d = Deal(3, 0, "", "free cheeseburgers!1", 500, 5, 4)
        bids = get_bids_for_deal(d)
        b = bids[0]
        assert(str(b) == "user: 2 deal: 3 old_price: 4.50 new_price: 4.00")
        b = bids[1]
        assert(str(b) == "user: 2 deal: 3 old_price: 4.00 new_price: 3.99")
        """


    def test_get_bids_for_user(self):
        self.setup_test_data()

        # try getting with user id
        bids = get_bids_for_user(2)
        b = bids[0]
        assert(str(b) == "user: 2 deal: 3 old_price: 4.50 new_price: 4.00")
        b = bids[1]
        assert(str(b) == "user: 2 deal: 3 old_price: 4.00 new_price: 3.99")
    
        # now try getting with a user object
        u = User(2, "a", "b", Sex.MALE, 0, "a@a.com", "", "512-123-4557", "1906-11-29", "", "fb001", "")
        bids = get_bids_for_user(u)
        b = bids[0]
        assert(str(b) == "user: 2 deal: 3 old_price: 4.50 new_price: 4.00")
        b = bids[1]
        assert(str(b) == "user: 2 deal: 3 old_price: 4.00 new_price: 3.99")
        

    def test_get_bids_for_user_and_deal(self):
        self.setup_test_data()

        # try getting with user id, deal id
        bids = get_bids_for_user_and_deal(2, 3)
        b = bids[0]
        assert(str(b) == "user: 2 deal: 3 old_price: 4.50 new_price: 4.00")
        b = bids[1]
        assert(str(b) == "user: 2 deal: 3 old_price: 4.00 new_price: 3.99")

        """
        TODO fix this call to the deal constructor
        # now try getting with user object and deal object
        d = Deal(3, 0, "", "free cheeseburgers!1", 500, 5, 4)
        u = User(2, "a", "b", Sex.MALE, 0, "a@a.com", "", "512-123-4557", "1906-11-29", "")
        bids = get_bids_for_user_and_deal(u, d)
        b = bids[0]
        assert(str(b) == "user: 2 deal: 3 old_price: 4.50 new_price: 4.00")
        b = bids[1]
        assert(str(b) == "user: 2 deal: 3 old_price: 4.00 new_price: 3.99")
        """

if __name__ == "__main__":
    unittest.main() # run all tests
