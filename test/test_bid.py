import unittest
import sys
sys.path.append("./src/")
from core.bid import *
from time import time

class TestBid(unittest.TestCase):

    def test1(self):
        t = time()
        b = Bid(1, False, 2, 3, 4.50, 4.00, 5, 'query1', t)
        assert(str(b) == "-- bid: user: 2 deal: 3 old_price: 4.50 new_price: 4.00")


    def test2(self):
        t = time()
        b = Bid(1, False, 2, 3, 4.50, 4.00, 5, 'query1', t)
        r = b.as_json_for_insertion()
        assert 'user_id' in r
        assert 'deal_id' in r
        assert 'old_price' in r
        assert 'new_price' in r

if __name__ == "__main__":
    unittest.main() # run all tests
