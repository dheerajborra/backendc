
/**
 * Module dependencies.
 */

var java = require("java");

var express = require('express')
  , app = express()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server)
  // , auth = require('./auth')
  // , python = require('node-python');
/*
var sys = python.import('sys');
sys.path.append('./src');
var mysqlDB = python.import('db_api.mysql_api');
*/

// allow CORS for development
var allowCrossDomain = function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	//
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
	// intercept OPTIONS method
	if ('OPTIONS' == req.method) {
	res.send(200);
	}
	else {
	next();
	}
};
app.use(allowCrossDomain);

app.use(express.static(__dirname + '/public'));

server.listen(8181);

app.get('/', function (req, res) {
  res.send("You found a cool place (test bid sent to all)");
  io.sockets.emit('updateBid', {id: "8", price_now: 24, price_prev: 25});
});

// app.get('/auth', auth.check);

function performBid(data, socket) {
	return {id: data.id, price_now: data.price_now-1, price_prev: data.price_now};
}

var clients = {};

io.sockets.on('connection', function (socket) {
	clients[socket.id] = socket;
	socket.username = "anon";

	socket.on('initialize', function (auth) {
		socket.username = getUserName(auth);
	});

	socket.on('favorite', function (data) {
		//perform favorite
		console.log("Favorite: " + data + " for " + socket.username);
	});

	socket.on('trash', function (data) {
		//perform trash
		console.log("Trash: " + data + " for " + socket.username);
	});

	socket.on('bid', function (data) {
		//perform bid
		var newData = performBid(data, socket);
		console.log("Bid: " + newData + " for " + socket.username);
		socket.emit('updateBid', newData);
	});

	socket.on('disconnect', function () {
		delete clients[socket.id];
	});
});

function getUserName (auth) {
	return "John Doe";
}
