# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field
from scrapy.contrib.loader.processor import MapCompose, Join
from w3lib.html import replace_escape_chars
import unicodedata
import re

class RestaurantItem(Item):
    #Restaurant details to be scraped
    name = Field()
    categories = Field()
    address = Field()
    number = Field()
    rating = Field()
    price_range = Field()
    hours = Field()
    url = Field()
    good_for_kids = Field()
    credit_cards = Field()
    parking = Field()
    attire = Field()
    groups = Field()
    reservations = Field()
    delivery = Field()
    takeout = Field()
    waiter_service = Field()
    outdoor_seating = Field()
    wifi = Field()
    good_for = Field()
    alcohol = Field()
    noise_level = Field()
    ambience = Field()
    has_tv = Field()
    caters = Field()
    wheelchair_accessible = Field()
    dogs_allowed = Field()
    

def is_phone(address):
    phone_num_re = re.compile(r'\d{3}-\d{3}-\d{4}')
    return phone_num_re.match(address) is not None


# function that we'll pass to reduce function. acts like a modified version of join
def reduce_f(x,y):
    if len(y) == 0:
        return x if x.endswith('|') or len(x) == 0 else x + ' |'
    else:
        return x + ' ' + y


def debug(v):
    print "debug: %s" % v
    return v
    

class DealItem(Item):


    def strip_phone(address):
        return "" if is_phone(address) else address

    def extract_phone(address):
        return address if is_phone(address) else ""


    def filtered_join_phones(lst):
        lst = filter(lambda x: x is not '', lst)
        return " | ".join(lst)

    # some of the details have artifacts from line breaks and other whitespace in the html.
    # get rid of these
    def strip_spaces(lst):
        lst = " ".join(lst).encode('ascii', 'ignore')
        s = re.compile(r'\s{2,}')
        return s.sub(' ', lst)

    # lst is a list of lines from addresses. 
    # there can be multiple addresses, but individual addresses are separated by empty
    # strings in the list.
    # take this list, and return a pipe-delimited list of addresses more suitable for
    # storage
    def join_addrs(lst):
        addrs = reduce(reduce_f, lst, '')
        return addrs[:-1].strip()

    merchant_name = Field()
    description = Field()
    price = Field()
    value = Field()
    saving = Field()
    discount = Field()
    # TODO there are extra spaces here
    details = Field( output_processor = strip_spaces)
    options = Field()
    address = Field(
            input_processor = MapCompose(lambda v: v.strip(),
                                         replace_escape_chars,
                                         lambda v: v.encode('ascii', 'ignore'),
                                         strip_phone),
            output_processor = join_addrs)

    phone_number = Field( 
            input_processor = MapCompose(lambda v: v.strip(), 
                                         replace_escape_chars,
                                         lambda v: v.encode('ascii', 'ignore'),
                                         extract_phone),
            output_processor = filtered_join_phones)
