from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.loader import XPathItemLoader
from scrapy.contrib.loader.processor import Compose, MapCompose, Join
from scrapy.http import Request
from w3lib.html import replace_escape_chars

from azulcrawler.items import RestaurantItem

import urlparse
import re


class YelpSpider(BaseSpider):
    name = 'yelp'
    allowed_domains = ['yelp.com']
    start_urls = ['http://www.yelp.com/c/austin/restaurants']    #Root urls for spider
    
    #Returns a list of all categories of restaurants within a particular city
    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        category_list = hxs.select('//div[@id = "subcategories-list"]/div/ul/li/ul[@class = "column-set"]/li/a/@href').extract()
        print "categories: %s " % category_list
        for category in category_list:
            yield Request(urlparse.urljoin('http://yelp.com', category), callback = self.parse_restaurant)

    #Moves from a mosaic display to a list display
    def parse_restaurant(self, response):
        hxs = HtmlXPathSelector(response)
        next_restaurant = hxs.select('//a[@class = "link-pill"]/@href').extract()
        if next_restaurant:
            yield Request(urlparse.urljoin('http://yelp.com', next_restaurant[0]) , callback = self.parse_list)
        else:
            print "YOOOOO"
            yield Request(response.url, callback = self.parse_page)

    #Extract a list of all the pages of restaurants
    def parse_list(self, response):
        hxs = HtmlXPathSelector(response)
        page_list = hxs.select('//a[@class = "page-option available-number"]/@href').extract()
        for nextpage in page_list:
            yield Request(urlparse.urljoin('http://yelp.com', nextpage), callback = self.parse_page)

    #Extract a list of all restaurants within a page
    def parse_page(self, response):
        print "MA NIGGGG"
        hxs = HtmlXPathSelector(response)
        for url in hxs.select('//span[@class = "indexed-biz-name"]/a/@href').extract():
            yield Request(urlparse.urljoin('http://yelp.com', url), callback = self.parse_item)

    #Scrapes data within a page and assigns it to the Item fields
    def parse_item(self, response):
        l = XPathItemLoader(item = RestaurantItem(), response=response)

        l.default_input_processor = MapCompose(lambda v: v.strip(), 
                                                replace_escape_chars)
        l.default_output_processor = Compose(Join(), lambda v: v.strip().encode('ascii', 'ignore'))

        l.add_xpath('name', '//h1/text()')
        l.add_xpath('number', '//span[@id = "bizPhone"]/text()')
        l.add_xpath('categories', '//span[@itemprop = "title"]/text()')
        l.add_xpath('address', '//span[@itemprop="streetAddress"]/text()')
        l.add_xpath('address', '//span[@itemprop="addressLocality"]/text()')
        l.add_xpath('address', '//span[@itemprop="addressRegion"]/text()')
        l.add_xpath('address', '//span[@itemprop="postalCode"]/text()')
        l.add_xpath('rating', '//div[@itemprop="aggregateRating"]/div/meta/@content')
        l.add_xpath('price_range', '//span[@id="price_tip"]/text()')
        l.add_xpath('url', '//div[@id = "bizUrl"]/a/text()')
        l.add_xpath('hours', '//p[@class = "hours"]/text()')
        l.add_xpath('good_for_kids', '//dd[@class = "attr-GoodForKids"]/text()')
        l.add_xpath('credit_cards', '//dd[@class = "attr-BusinessAcceptsCreditCards"]/text()')
        l.add_xpath('parking', '//dd[@class = "attr-BusinessParking"]/text()')
        l.add_xpath('attire', '//dd[@class = "attr-RestaurantsAttire"]/text()')
        l.add_xpath('groups', '//dd[@class = "attr-RestaurantsGoodForGroups"]/text()')
        l.add_xpath('reservations', '//dd[@class = "attr-RestaurantsReservations"]/text()')
        l.add_xpath('delivery', '//dd[@class = "attr-RestaurantsDelivery"]/text()')
        l.add_xpath('takeout', '//dd[@class = "attr-RestaurantsTakeOut"]/text()')
        l.add_xpath('outdoor_seating', '//dd[@class = "attr-OutdoorSeating"]/text()')
        l.add_xpath('wifi', '//dd[@class = "attr-WiFi"]/text()')
        l.add_xpath('good_for', '//dd[@class = "attr-GoodForMeal"]/text()')
        l.add_xpath('alcohol', '//dd[@class = "attr-Alcohol"]/text()')
        l.add_xpath('noise_level', '//dd[@class = "attr-NoiseLevel"]/text()')
        l.add_xpath('ambience', '//dd[@class = "attr-Ambience"]/text()')
        l.add_xpath('has_tv', '//dd[@class = "attr-HasTV"]/text()')
        l.add_xpath('caters', '//dd[@class = "attr-Caters"]/text()')
        l.add_xpath('wheelchair_accessible', '//dd[@class = "attr-WheelchairAccessible"]/text()')
        l.add_xpath('dogs_allowed', '//dd[@class = "attr-DogsAllowed"]/text()')
        
        return l.load_item()
