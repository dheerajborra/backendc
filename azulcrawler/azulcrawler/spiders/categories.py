from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.loader import XPathItemLoader
from scrapy.contrib.loader.processor import Compose, MapCompose, Join
from scrapy.http import Request
from w3lib.html import replace_escape_chars

from azulcrawler.items import RestaurantItem

import urlparse
import re


class CategorySpider(BaseSpider):
    name = 'categories'
    allowed_domains = ['yelp.com']
    start_urls = ['http://www.yelp.com/c/austin/restaurants']    #Root urls for spider

    def parse(self, response):
        l = XPathItemLoader(item = RestaurantItem(), response = response)
        l.default_input_processor = MapCompose(lambda v: v.strip(), 
                                                replace_escape_chars)
        # scrape a pipe-delimited list of categories
        l.default_output_processor = Compose(Join('|'), lambda v: v.strip().encode('ascii', 'ignore'))
        l.add_xpath('categories', '//div[@id = "subcategories-list"]/div/ul/li/ul[@class = "column-set"]/li/a/text()')
        return l.load_item()
