from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.loader import XPathItemLoader
from scrapy.http import Request


from azulcrawler.items import RestaurantItem
from random import randint

import urlparse
import re

throttle = True
proxies = ['219.141.230.151:3128', '24.172.34.114:8181', '173.213.230.3:80', '173.213.96.229:3127', '173.213.113.111:8089',
        '72.9.159.87:8089', '66.35.68.146:3127', '173.208.196.212:8888', '168.63.36.204:80', '69.15.234.90:45254']

def get_random_proxy():
    ret = {'proxy':proxies[randint(0, len(proxies)-1)]}
    print ret
    return {}

class RestaurantSpider(BaseSpider):
    name = 'urbanspoon'
    allowed_domains = ['urbanspoon.com']
    start_urls = ['http://www.urbanspoon.com/c/11/Austin-restaurants.html']


    # start by extracting a list of all the neighborhoors
    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        neighborhoods = hxs.select('//div[@class="title"]/a[@data-ga-action="hood"]/@href').extract()
        if throttle:
            neighborhoods = neighborhoods[:3]

        print neighborhoods

        for neighborhood in neighborhoods:
            yield Request(urlparse.urljoin('http://urbanspoon.com', neighborhood), callback=self.parse_neighborhood_page)


    def parse_neighborhood_page(self, response):
        hxs = HtmlXPathSelector(response)
        restaurants = hxs.select('//a[@class="resto_name ga_event"]/@href').extract()

        if throttle:
            restaurants = restaurants[:3]

        print restaurants

        # parse all the restaurants on this page
        for restaurant in restaurants:
            yield Request(urlparse.urljoin('http://urbanspoon.com', restaurant), callback = self.parse_restaurant) 

        # advance the page
        next_page = hxs.select('//a[@class="next_page"]/@href').extract()
        if len(next_page) > 0:
            yield Request(urlparse.urljoin('http://urbanspoon.com', next_page[0]), callback = self.parse_neighborhood_page)


    def parse_restaurant(self, response):
        l = XPathItemLoader(item = RestaurantItem(), response=response)
        l.add_xpath('name', '//h1/text()')
        l.add_xpath('number', '//h3[@class = "phone tel"]/text()')
        l.add_xpath('categories', '//a[@data-ga_action = "explore-resto-cuisinitemprope"]/text()')
        l.add_xpath('address', '//span[@class="street-address"]/text()')
        l.add_xpath('address', '//span[@class="locality"]/text()')
        l.add_xpath('address', '//span[@class="region"]/text()')
        #l.add_xpath('address', '//div[@class = "address"/a/text()')
        l.add_xpath('rating', '//div[@class="average digits percent-text-rating"]/text()')
        l.add_xpath('rating', '//div[@class="percent"]/text()')
        l.add_xpath('rating', '//div[@class="total num-votes"]/text()')
        l.add_xpath('price_range', '//span[@class="price"]/a/text()')
        #l.add_xpath('url', '//a[@class = "icon website ga_event"]/@href()')
        return l.load_item()
