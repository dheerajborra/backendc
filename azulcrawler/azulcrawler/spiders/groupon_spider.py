from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.loader import XPathItemLoader
from scrapy.contrib.loader.processor import Compose, MapCompose, Join
from scrapy.http import Request
from w3lib.html import replace_escape_chars

from azulcrawler.items import DealItem

import urlparse
import re


class GrouponSpider(BaseSpider):
    name = 'groupon'
    allowed_domains = ['groupon.com']
    start_urls = ["http://www.groupon.com/local/austin/food-and-drink"]

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        deals = hxs.select('//a[@class="deal-link"]/@href').extract()

        for deal in deals:
            yield Request(urlparse.urljoin('http://groupon.com', deal), callback = self.parse_deal)
    
    def parse_deal(self, response):
        l = XPathItemLoader(item = DealItem(), response=response)
        l.default_input_processor = MapCompose(lambda v: v.strip(), 
                                                replace_escape_chars)
        l.default_output_processor = Compose(Join(), lambda v: v.strip().encode('ascii', 'ignore'))
        l.add_xpath('merchant_name', '//h2[@class = "deal-title"]/text()')
        l.add_xpath('description', '//h3[@class = "deal-subtitle"]/text()')
        l.add_xpath('price', '//span[@class = "price"]/text()')
        l.add_xpath('value', '//td[@id ="discount-value"]/text()')
        l.add_xpath('saving', '//td[@id = "discount-you-save"]/text()')
        l.add_xpath('discount', '//td[@id = "discount-percent"]/text()')
        l.add_xpath('details', '//div[@class = "fine-print six columns"]/p/text()')
        l.add_xpath('options', '//article[@class="eight columns pitch"]/h4/text()')
        l.add_xpath('options', '//article[@class="eight columns pitch"]/ul/li/text()')
        l.add_xpath('address', '//div[@class = "address"]/text()')
        l.add_xpath('phone_number', '//div[@class = "address"]/text()')
        return l.load_item()
