CREATE TABLE if not exists users 
(
  id INT unsigned NOT NULL AUTO_INCREMENT,
  firstname varchar(32),
  lastname varchar(32),
  sex enum('F', 'M', 'U'),
  karma INT unsigned, 
  email varchar(32) UNIQUE NOT NULL,
  password CHAR(60) BINARY NOT NULL,
  phonenumber varchar(16),
  birthdate date,
  flags tinyint default 0,
  facebook_id varchar(32) UNIQUE,
  facebook_token tinytext,
  stripe_customer varchar(32),
  PRIMARY KEY (id)
);

CREATE TABLE if not exists usercards
(
  card_id INT unsigned NOT NULL AUTO_INCREMENT,
  user_id INT unsigned NOT NULL,
  card_token varchar(64) NOT NULL,
  last_four char(4) NOT NULL,
  expiration_month INT unsigned NOT NULL,
  expiration_year INT unsigned NOT NULL,
  card_type varchar(32) NOT NULL,
  PRIMARY KEY (card_id)
);

CREATE TABLE if not exists purchases
(
  purchase_id INT unsigned NOT NULL AUTO_INCREMENT,
  user_id INT unsigned NOT NULL,
  deal_id varchar(32) NOT NULL,
  address varchar(64),
  category varchar(32),
  fineprint varchar(64),
  img varchar(128),
  voucher_img varchar(64),
  voucher_code varchar(32),
  location_lat float,
  location_lon float,
  merchant_id INT unsigned,
  merchant varchar(32),
  price_now float NOT NULL,
  price_reg float,
  purchase_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (purchase_id)
);

CREATE TABLE if not exists vouchers
(
  voucher_id INT unsigned NOT NULL AUTO_INCREMENT,
  deal_id varchar(32) NOT NULL,
  voucher_img varchar(64),
  voucher_code varchar(32),
  voucher_used BOOL default false,
  PRIMARY KEY (voucher_id)
);

CREATE TABLE if not exists merchants
(
  id INT unsigned NOT NULL AUTO_INCREMENT,
  name varchar(32) NOT NULL,
  address varchar(64),
  latitude float,
  longitude float,
  phonenumber varchar(16),
  email varchar(32) UNIQUE NOT NULL,
  rating float,
  price float, 
  flags tinyint default 0,
  PRIMARY KEY (id)
);

CREATE TABLE if not exists queries
(
  id INT unsigned NOT NULL AUTO_INCREMENT,
  user_id INT unsigned,
  deal_id varchar(32),
  num_iterations INT unsigned, 
  query_str varchar(128),
  PRIMARY KEY (id)
);

CREATE TABLE if not exists favorite_deals
(
  query_id varchar(32), 
  user_id INT unsigned NOT NULL, 
  deal_id varchar(32) NOT NULL
);

CREATE TABLE if not exists favorite_merchants
(
  query_id varchar(32), 
  user_id INT unsigned NOT NULL, 
  merchant_id INT unsigned NOT NULL
);

CREATE TABLE if not exists trashed_deals
(
  query_id varchar(32), 
  user_id INT unsigned NOT NULL, 
  deal_id varchar(32) NOT NULL
); 

CREATE TABLE if not exists trashed_merchants
(
  query_id varchar(32), 
  user_id INT unsigned NOT NULL, 
  merchant_id INT unsigned NOT NULL
); 

CREATE TABLE if not exists suggestion
(
  user_id INT unsigned NOT NULL,
  deal_id varchar(32) NOT NULL
);

CREATE TABLE if not exists feedback
(
  user_id INT unsigned,
  positive_comment TEXT,
  negative_comment TEXT,
  other_comment TEXT
);

CREATE TABLE if not exists bid_history
(
  id INT unsigned NOT NULL AUTO_INCREMENT,
  user_id INT unsigned NOT NULL,
  query_id varchar(32) NOT NULL,
  deal_id varchar(32) NOT NULL,
  old_price float NOT NULL,
  new_price float NOT NULL,
  inventory INT unsigned,
  initial_bid boolean,
  time timestamp,
  flags tinyint default 0,
  PRIMARY KEY(id)
);
